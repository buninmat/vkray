#include "geometry.h"
#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>


namespace std {
	template<> struct hash<Vertex> {
		size_t operator()(const Vertex& vert) const noexcept {
			return hash<glm::vec3>()(vert.pos)
				//^ (hash<glm::vec3>()(vert.color) << 1)
				^ (hash<glm::vec3>()(vert.normal) << 1)
				^ (hash<glm::vec2>()(vert.uv) << 2);
		}
	};
}

bool Vertex::operator == (const Vertex& other) const {
	return pos == other.pos
		//&& color == other.color
		&& normal == other.normal
		&& uv == other.uv;
}

static const std::string modelFolder = "models";
static const std::string dumpFolder = "dumps";

static inline const std::string vertDumpPath(std::string& name) {
	return dumpFolder + "/" + name + "-vert.dump";
}

static inline const std::string indDumpPath(std::string& name) {
	return dumpFolder + "/" + name + "-ind.dump";
}

void GeometryObject::saveDump() {

	if (!std::filesystem::exists(dumpFolder)) {
		std::filesystem::create_directories(dumpFolder);
	}

	writeBinary(vertDumpPath(name),
		reinterpret_cast<const char*>(vertices.data()),
		vertices.size() * sizeof(Vertex));

	writeBinary(indDumpPath(name),
		reinterpret_cast<const char*>(indices.data()),
		indices.size() * sizeof(uint32_t));
}

bool GeometryObject::loadDump() {
	std::string dumpVert = vertDumpPath(name);
	std::string dumpInd = indDumpPath(name);

	if (!std::filesystem::exists(dumpVert) || !std::filesystem::exists(dumpInd)) {
		return false;
	}
	std::vector<char> vertBytes = readBinary(dumpVert);
	std::vector<char> indBytes = readBinary(dumpInd);
	vertices.resize(vertBytes.size() / sizeof(Vertex));
	indices.resize(indBytes.size() / sizeof(uint32_t));
	memcpy(vertices.data(), vertBytes.data(), vertBytes.size());
	memcpy(indices.data(), indBytes.data(), indBytes.size());
}

void GeometryObject::loadObj() {
	std::string modelPath = modelFolder + "/" + name + ".obj";
	if (!std::filesystem::exists(modelPath)) {
		std::cout << "Model .obj file cannot be found" << std::endl;
		return;
	}

	tinyobj::ObjReaderConfig readerConfig;
	readerConfig.mtl_search_path = modelFolder;// +"/";
	readerConfig.triangulate = true;
	
	tinyobj::ObjReader reader;
	if (!reader.ParseFromFile(modelPath, readerConfig)) {
		if (!reader.Error().empty()) {
			std::cout << reader.Error() << std::endl;
		}
		throw std::runtime_error("Failed to parse model file");
	}
	if (!reader.Warning().empty()) {
		std::cout << reader.Warning() << std::endl;
	}

	auto& attrib = reader.GetAttrib();
	auto& shapes = reader.GetShapes();

	std::unordered_map<Vertex, uint32_t> vertexInds;

	for (const auto& index : shapes[0].mesh.indices) {
		Vertex vertex{};
		size_t vind = index.vertex_index;
		memcpy(&vertex.pos, &attrib.vertices[3 * vind], 3 * sizeof(float));
		//memcpy(&vertex.color, &attrib.colors[3 * vind], 3 * sizeof(float));
		memcpy(&vertex.normal, &attrib.normals[3 * (size_t)index.normal_index], 3 * sizeof(float));
		if (index.texcoord_index != -1) {
			memcpy(&vertex.uv, &attrib.texcoords[2 * (size_t)index.texcoord_index], 2 * sizeof(float));
		}

		if (vertexInds.find(vertex) == vertexInds.end()) {
			vertexInds[vertex] = vertices.size();
			vertices.push_back(vertex);
		}
		indices.push_back(vertexInds[vertex]);
	}

	auto& mats = reader.GetMaterials();
	if (!mats.empty()) {
		material.diffuseTexture = std::make_unique<Texture>(mats[0].diffuse_texname);
	}
}

GeometryObject* GeometryObject::makeCube() {
	auto cube = new GeometryObject();
	std::vector<glm::vec3> vertices = {
		{1.0f, 1.0f, 1.0f},
		{1.0f, 1.0f, -1.0f},
		{1.0f, -1.0f, 1.0f},
		{1.0f, -1.0f, -1.0f},
		{-1.0f, 1.0f, 1.0f},
		{-1.0f, 1.0f, -1.0f},
		{-1.0f, -1.0f, 1.0f},
		{-1.0f, -1.0f, -1.0f}
	};
	for (const auto& pos : vertices) {
		cube->vertices.push_back({ glm::vec4(pos, 0.0f), {glm::sign(pos.x), 0.0f, 0.0f, 0.0f} });
		cube->vertices.push_back({ glm::vec4(pos, 0.0f), {0.0f, glm::sign(pos.y), 0.0f, 0.0f} });
		cube->vertices.push_back({ glm::vec4(pos, 0.0f), { 0.0f, 0.0f, glm::sign(pos.z), 0.0f} });
	}
	cube->indices.resize(36);
	for (int i = 0; i < 6; i++) {
		GLuint side[4];
		int cnt = 0;
		for (int j = 0; j < cube->vertices.size(); j++) {
			int coord = glm::value_ptr(cube->vertices[j].normal)[(int)(i / 2)];
			if (coord != 0 && ((coord == 1) == i % 2)) {
				side[cnt] = j;
				cnt++;
			}
		}
		memcpy(cube->indices.data() + i * 6, side, sizeof(GLuint) * 3);
		memcpy(cube->indices.data() + i * 6 + 3, side + 1, sizeof(GLuint) * 3);
	}
	return cube;
}

GeometryObject* GeometryObject::makeUVSphere(float radius, int steps) {
	auto* uvSphere = new GeometryObject();
	//poles
	uvSphere->vertices.push_back({ {0.0f, radius, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f, 0.0f} });
	uvSphere->vertices.push_back({ {0.0f, -radius, 0.0f, 0.0f}, {0.0f, -1.0f, 0.0f, 0.0f} });
	//TODO
	return uvSphere;
}

GeometryObject* GeometryObject::makeTorus(float radius, float ringRadius, int uSteps, int vSteps) {
	auto* torus = new GeometryObject();
	float uStride = glm::two_pi<float>() / uSteps;
	float vStride = glm::two_pi<float>() / vSteps;
	for (int i = 0; i < uSteps; i++) {
		float u = i * uStride;
		glm::vec4 ringOrigin = glm::vec4{ glm::cos(u), glm::sin(u), 0.0f, 0.0f } *radius;
		for (int j = 0; j < vSteps; j++) {
			float v = j * vStride;
			glm::vec4 normal = glm::vec4{ glm::cos(u) * glm::cos(v), glm::sin(u) * glm::cos(v), -glm::sin(v), 0.0f };
			Vertex vert{ normal * ringRadius + ringOrigin, normal };
			vert.uv.s = (float)i / uSteps;
			vert.uv.t = (float)j / vSteps;
			torus->vertices.push_back(vert);

			int i_next = (i + 1) % uSteps;
			int j_next = (j + 1) % vSteps;
			int v1 = i * vSteps + j;
			int v2 = i * vSteps + j_next;
			int v3 = i_next * vSteps + j;
			int v4 = i_next * vSteps + j_next;

			torus->indices.push_back(v1);
			torus->indices.push_back(v2);
			torus->indices.push_back(v4);

			torus->indices.push_back(v1);
			torus->indices.push_back(v4);
			torus->indices.push_back(v3);
		}
	}
	torus->material.diffuseTexture = std::make_unique<Texture>("metal-texture.jpg");
	return torus;
}

