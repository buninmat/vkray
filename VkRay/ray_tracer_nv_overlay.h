#pragma once
#include "ray_tracer_nv.h"

class RayTracerNVOverlay : public RayTracerNV
{

	struct OverlayObject : SceneObject {
		Buffer indexBuffer;
		Buffer vertexBuffer;

		std::unique_ptr<TextureImage> textureImage;

		OverlayObject(const GeometryObject& geometry) {
			
			createElementsBuffer(vertexBuffer, geometry.vertices,
				VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);
			createElementsBuffer(indexBuffer, geometry.indices,
				VK_BUFFER_USAGE_INDEX_BUFFER_BIT);
			if (geometry.material.diffuseTexture.get() != nullptr) {
				textureImage = std::make_unique<TextureImage>(device, *geometry.material.diffuseTexture, VK_FILTER_NEAREST);
			}
		}

		~OverlayObject() {
			indexBuffer.destroy(device);
			vertexBuffer.destroy(device);
		}

	};

	VkRenderPass renderPass;

	VkPipelineLayout pipelineLayout;

	VkDescriptorSetLayout discriptorSetLayout;
	VkDescriptorPool descriptorPool;
	std::vector<VkDescriptorSet> descriptorSets;

	VkOffset2D viewportOffset = { 0, 0 };

	VkPipeline graphicsPipeline;

	std::vector<VkImageView> swapchainImageViews;
	std::vector<VkFramebuffer> swapchainFramebuffers;

	std::unique_ptr<OverlayObject> textObject;

	std::unique_ptr<OverlayObject> backgObject;

	VkSampler rtImageSampler;

	bool hideInfo = false;

	void recordPipelineDrawCommands(VkCommandBuffer commandBuffer, int swapchainIndex) override;

	void printProfilingInfo() override;

	void createSwapchainDeps() override {
		RayTracerNV::createSwapchainDeps();
		createSwapchainOverlayDeps();
	};

	void onKeyAction(int key, int action) {
		updateRecursionDepth(key, action);
		updateMaxSamples(key, action);
		if (key == GLFW_KEY_SPACE && action == GLFW_PRESS) {
			hideInfo = !hideInfo;
		}
	}
	
	glm::vec2 unifScaleToWindow(glm::vec2 ext) {
		if (ext.x == 0 || ext.y == 0) { return ext; }
		auto winExtAbs = glm::vec2(winWidth, winHeight);
		int imax = winExtAbs.x > winExtAbs.y ? 0 : 1;
		float absMin = winExtAbs[imax] * ext[imax];
		float absOther = absMin * ext[1 - imax] / ext[imax];
		ext[1 - imax] = absOther / winExtAbs[1 - imax];
		return ext;
	}

	glm::vec2 preserveDefaultWinScale(glm::vec2 ext) {
		glm::vec2 abs = glm::vec2(ext.x * defaultWinWidth, ext.y * defaultWinHeight);
		return glm::vec2(abs.x / winWidth, abs.y / winHeight);
	}

	const int textFrameH = 300;
	const int textFrameW = 300;
	const glm::vec2 textBlockExtent = glm::vec2(0.3f, 0.6f);
	const glm::vec2 textBlockOffset = glm::vec2(0.4f, 0.7f);

	void createSwapchainOverlayDeps() {
		createImageViews();
		createGraphicsPipeline();
		createFramebuffers();

		createRTImageSampler();
		
		glm::vec2 extent = preserveDefaultWinScale(textBlockExtent);
		glm::vec2 offset = preserveDefaultWinScale(textBlockOffset) + glm::vec2(-1.0, -1.0);
		createTextObject(extent, offset);
			
		printProfilingInfo();

		createDescriptorPool();
		createDescriptorSets();
	}

	void cleanupSwapchainDeps() override {
		cleanupSwapchainOverlayDeps();
		RayTracerNV::cleanupSwapchainDeps();
	};

	void cleanupSwapchainOverlayDeps() {
		textObject.reset();

		vkDestroyDescriptorPool(device, descriptorPool, nullptr);

		vkDestroySampler(device, rtImageSampler, nullptr);

		for (auto fb : swapchainFramebuffers) {
			vkDestroyFramebuffer(device, fb, nullptr);
		}
		vkDestroyPipeline(device, graphicsPipeline, nullptr);
		vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
		for (auto imageView : swapchainImageViews) {
			vkDestroyImageView(device, imageView, nullptr);
		}
	}

	void createImageViews();

	void createRenderPass();

	void createDescriptorSetLayout();

	void createGraphicsPipeline();

	void createFramebuffers();

	void createDescriptorPool();

	void createDescriptorSets();

	void createBackgObject();

	void createTextObject(glm::vec2 extent, glm::vec2 offset);

	void createRTImageSampler();

public:
	RayTracerNVOverlay(Arguments arguments)
		: RayTracerNV(arguments) {
		rtParams.gammaCorrect = false;
		rtParams.invGammaCorrect = false;
		rtParams.readBGR = false;
		rtParams.writeBGR = false;
		createBackgObject();
		createRenderPass();
		createDescriptorSetLayout();
		createSwapchainOverlayDeps();
	}

	~RayTracerNVOverlay() {

		cleanupSwapchainOverlayDeps();

		backgObject.reset();

		vkDestroyRenderPass(device, renderPass, nullptr);

		vkDestroyDescriptorSetLayout(device, discriptorSetLayout, nullptr);
	}
};

