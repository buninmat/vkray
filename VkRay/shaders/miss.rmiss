#version 460
#extension GL_NV_ray_tracing : require
#extension GL_GOOGLE_include_directive : require

#include "../shaders_shared.h"

layout(location = 0) rayPayloadInNV Payload payload;

void main() {
	payload.hitT = -1.0;
}