#version 460

layout(location = 0) in vec2 uv;
//layout(location = 1) in vec3 color;

layout(location = 0) out vec4 outColor; 

layout(binding = 0) uniform sampler2D texSampler;

void main(){
	outColor = texture(texSampler, uv);
	//outColor = vec4(color, 0);//vec4(uv.x, uv.y, 0, 1);
}
