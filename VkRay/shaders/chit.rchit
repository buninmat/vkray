#version 460
#extension GL_NV_ray_tracing : require
#extension GL_EXT_nonuniform_qualifier : require
#extension GL_GOOGLE_include_directive : require

#include "../shaders_shared.h"

layout(location=0) rayPayloadInNV Payload payload;

hitAttributeNV vec2 hitAttr;

struct Vertex{
	vec4 pos;
	vec4 normal;
	vec4 uv;
};

layout(binding=3, std140) readonly buffer VertexBuffer{
	Vertex vertices[];
} vertexBuffers[];


layout(binding=4) readonly buffer IndexBuffer{
	uint indices[];
} indexBuffers[];

void main(){
	uint geomId = nonuniformEXT(gl_InstanceCustomIndexNV);
	ivec3 ind = ivec3(indexBuffers[geomId].indices[gl_PrimitiveID * 3],
					indexBuffers[geomId].indices[gl_PrimitiveID * 3 + 1],
					indexBuffers[geomId].indices[gl_PrimitiveID * 3 + 2]);
	vec3 n1 = vertexBuffers[geomId].vertices[ind.x].normal.xyz;
	vec3 n2 = vertexBuffers[geomId].vertices[ind.y].normal.xyz;
	vec3 n3 = vertexBuffers[geomId].vertices[ind.z].normal.xyz;

	vec2 uv1 = vertexBuffers[geomId].vertices[ind.x].uv.xy;
	vec2 uv2 = vertexBuffers[geomId].vertices[ind.y].uv.xy;
	vec2 uv3 = vertexBuffers[geomId].vertices[ind.z].uv.xy;

	vec3 bc = vec3(1.0f - hitAttr.x - hitAttr.y, hitAttr.x, hitAttr.y);
	payload.normal = normalize(n1 * bc.x + n2 * bc.y + n3 * bc.z);
	payload.uv = uv1 * bc.x + uv2 * bc.y + uv3 * bc.z; 
	payload.hitT = gl_HitTNV;
	payload.objId = geomId;
}