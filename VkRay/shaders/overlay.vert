#version 460

layout(location = 0) in vec3 pos;
//layout(location = 1) in vec3 norm;
//layout(location = 2) in vec3 uvIn;

layout(location = 0) out vec2 uv;
//layout(location = 1) out vec3 color;

void main() {
    gl_Position = vec4(pos, 1.0);
    
    vec2 uvs[] = {vec2(0, 1), vec2(1, 1), vec2(1, 0), vec2(0, 0)};

    uv = uvs[gl_VertexIndex];
}