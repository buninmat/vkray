#include "vkray.h"

std::string readText(std::string fname) {
	std::ifstream fstream(fname, std::ios::in);
	if (!fstream.is_open()) {
		throw std::runtime_error("Failed to open shader file " + fname);
	}
	return std::string(std::istreambuf_iterator<char>(fstream), std::istreambuf_iterator<char>());
}

std::vector<char> readBinary(const std::string& filename) {
	std::ifstream file(filename, std::ios::binary | std::ios::ate);
	if (!file.is_open()) {
		throw std::runtime_error("Failed to open binary file " + filename);
	}
	size_t fsize = file.tellg();
	file.seekg(0);
	std::vector<char> bytes(fsize);
	file.read(bytes.data(), fsize);
	file.close();
	return bytes;
}

void writeBinary(const std::string& filename, const char* data, size_t size) {
	std::ofstream file(filename, std::ios::binary);
	if (!file.is_open()) {
		throw std::runtime_error("Failed to open binary file " + filename);
	}
	file.write(data, size);
	file.close();
}

void ulongToStr(std::stringstream& stream, unsigned long number, char separator) {
	unsigned long quot = number / 1000;
	if (quot != 0) {
		ulongToStr(stream, quot);
		stream << separator;
	}
	char str[4];
	sprintf_s(str, quot == 0 ? "%d" : "%03d", number % 1000);
	stream << str;
}

void tolower(std::string &str) {
	std::transform(str.begin(), str.end(), str.begin(), [](unsigned char c)->unsigned char { return std::tolower(c); });
}
