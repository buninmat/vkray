#pragma once
#include <string>
#include <vector>

std::string readText(std::string fname);

std::vector<char> readBinary(const std::string& filename);

void writeBinary(const std::string& filename, const char* data, size_t size);

void ulongToStr(std::stringstream& stream, unsigned long number, char separator = ' ');

void tolower(std::string&);