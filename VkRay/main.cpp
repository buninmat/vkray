#include "vkray.h"
#include "ray_tracer_nv.h"
#include "ray_tracer_nv_overlay.h"

int main(int argc, char** argv) {
	Arguments arguments{ SceneType::BUDDHAS, true, false, false, true, false, 100, 3, 0, -1, 3 };
	bool overlay = true;
	enum ExpArg{NONE, SCENE, SAMPLES, MIN_BOUNCES, MAX_BOUNCES, FRAMES, IMAGE };
	ExpArg exp = NONE;
	for (int i = 1; i < argc; ++i) {
		std::string arg = argv[i];
		tolower(arg);
		if (exp == ExpArg::SCENE) {
			if (arg == "monkeys") {
				arguments.sceneType = SceneType::MONKEYS;
			}
			if (arg == "bunny") {
				arguments.sceneType = SceneType::BUNNY;
			}
			if (arg == "buddhas") {
				arguments.sceneType = SceneType::BUDDHAS;
			}
			if (arg == "buddhas_inst") {
				arguments.sceneType = SceneType::INSTANCED_BUDDHAS;
			}
			exp = ExpArg::NONE;
			continue;
		}
		int argInt = atoi(arg.c_str());
		if(argInt > 0){
			if (exp == ExpArg::MAX_BOUNCES) {
				arguments.maxBounces = argInt;
			}
			if (exp == ExpArg::MIN_BOUNCES) {
				arguments.minBounces = argInt;
			}
			if (exp == ExpArg::SAMPLES) {
				arguments.numSamples = argInt;
			}
			if (exp == ExpArg::FRAMES) {
				arguments.maxFrames = argInt;
			}
			exp = ExpArg::NONE;
			continue;
		}
		if (exp == ExpArg::IMAGE) {
			arguments.imagePath = arg;
			exp = ExpArg::NONE;
			continue;
		}
		if (arg == "--samples") {
			exp = ExpArg::SAMPLES;
		}
		if (arg == "--scene") {
			exp = ExpArg::SCENE;
		}
		if (arg == "--bouncesmin") {
			exp = ExpArg::MIN_BOUNCES;
		}
		if (arg == "--bouncesmax") {
			exp = ExpArg::MAX_BOUNCES;
		}
		if (arg == "--nolights") {
			exp = ExpArg::NONE;
			arguments.useLightSources = false;
		}
		if (arg == "--frames") {
			exp = ExpArg::FRAMES;
		}
		if (arg == "--image") {
			exp = ExpArg::IMAGE;
		}
		if (arg == "--silent" || arg == "-s") {
			exp = ExpArg::NONE;
			arguments.silent = true;
		}
		if (arg == "--profile" || arg == "-p") {
			exp = ExpArg::NONE;
			arguments.profile = true;
		}
		if (arg == "--noraycount") {
			exp = ExpArg::NONE;
			arguments.countRays = false;
		}
		if (arg == "--nooverlay") {
			exp = ExpArg::NONE;
			overlay = false;
		}
		if (arg == "--offscreen") {
			exp = ExpArg::NONE;
			arguments.offscreen = true;
		}
	}
	if (overlay) {
		RayTracerNVOverlay renderer(arguments);
		renderer.run();
	}
	else {
		RayTracerNV renderer(arguments);
		renderer.run();
	}

	return EXIT_SUCCESS;
}
