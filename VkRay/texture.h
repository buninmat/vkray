#pragma once
#include "vkray.h"

struct Texture {

	unsigned char* data;
	int width;
	int height;
	int nChannels = 4;

	bool stbi = false;

	Texture() {};

	Texture(std::string file, bool hdr);

	Texture(std::string file) :Texture(file, false) {}

	~Texture();
};
