#include "vulkan_base.h"

VulkanBase* VulkanBase::runningInstance = nullptr;

static double prevXPos, prevYPos;
static bool prevPosValid = false;


VulkanBase::VulkanBase(Arguments arguments) : arguments(arguments) {
	runningInstance = this;
	log.silent = arguments.silent;
	initWindow();
	initVulkan();
}

void VulkanBase::initWindow() {
	glfwInit();
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API); // do not create OGL context

	window = glfwCreateWindow(winWidth, winHeight, "VkRay", nullptr, nullptr);

	glfwSetCursorPosCallback(window, mouseMove);
	glfwSetScrollCallback(window, mouseScroll);
	glfwSetWindowSizeCallback(window, resizedCallback);
	glfwSetKeyCallback(window, keyPressed);
}

void VulkanBase::initVulkan() {
	createInstance();
	createSurface();
	pickDevice();
	createLogicalDevice();
	createSwapchain(false);
	createCommandPool();
	createTimestampQueryPool(timeQueryPool);
}

void VulkanBase::runInit() {
	createCommandBuffers();
	createSyncObjects();
}


VulkanBase::~VulkanBase() {
	vkDestroyQueryPool(device, timeQueryPool, nullptr);

	scene.reset();

	vkDestroySwapchainKHR(device, swapchain, nullptr);

	vkDestroyDevice(device, nullptr);
	vkDestroySurfaceKHR(instance, surface, nullptr);
	vkDestroyInstance(instance, nullptr);
	glfwDestroyWindow(window);
	glfwTerminate();
}

void VulkanBase::runCleanup() {
	cleanupSyncObjects();
	vkDestroyCommandPool(device, commandPool, nullptr);
}

void VulkanBase::cleanupSyncObjects() {
	for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
		vkDestroySemaphore(device, swapchainImageSemaphores[i], nullptr);
		vkDestroySemaphore(device, renderSemaphores[i], nullptr);
		vkDestroyFence(device, frameFences[i], nullptr);
	}
	renderFences.resize(0);
}


void VulkanBase::run() {

	runInit();

	renderStart = std::chrono::high_resolution_clock::now();

	while (!glfwWindowShouldClose(runningInstance->window)
		&& (renderState.framesCnt != arguments.maxFrames) 
		&& !renderTerminated) {
		glfwPollEvents();
		drawFrame();
	}
	vkDeviceWaitIdle(runningInstance->device);

	runCleanup();
	runningInstance = nullptr;
}



void VulkanBase::mouseMove(GLFWwindow* window, double xpos, double ypos) {
	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) != GLFW_PRESS) {
		prevPosValid = false;
		return;
	}
	if (xpos < 0 || ypos < 0 || xpos > runningInstance->winWidth || ypos > runningInstance->winHeight) {
		return;
	}
	if (prevPosValid) {
		glm::vec2 delta = glm::vec2(xpos - prevXPos, ypos - prevYPos) / glm::vec2(runningInstance->winWidth, runningInstance->winHeight);
		runningInstance->scene->camera->mouseMove(delta, glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS);
	}
	prevXPos = xpos;
	prevYPos = ypos;
	prevPosValid = true;
}

void VulkanBase::createInstance() {
	bool useValidationLayers = false;
	if (enableValidationLayers) {
		if (checkValidationLayersSupport()) {
			useValidationLayers = true;
		}
		else {
			log.warning("Requested validation layers are not supported!");
		}
	}

	VkApplicationInfo appInfo{}; //null-init, optional
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = "VkRay";
	appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.apiVersion = VK_API_VERSION_1_2;

	VkInstanceCreateInfo instInfo{};
	instInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instInfo.pApplicationInfo = &appInfo;
	//extentions
	uint32_t extCount = 0;
	const char** extensions_raw = glfwGetRequiredInstanceExtensions(&extCount);
	std::vector<const char*> extensions(extensions_raw, extensions_raw + extCount);
	log.check_failure(checkGlobalExtsSupport(extensions), "Required GLFW extensions are not supported");
	instInfo.ppEnabledExtensionNames = extensions_raw;
	instInfo.enabledExtensionCount = extCount;
	//layers
	if (useValidationLayers) {
		instInfo.ppEnabledLayerNames = validationLayers.data();
		instInfo.enabledLayerCount = validationLayers.size();
	}

	log.check_failure(vkCreateInstance(&instInfo, nullptr, &instance), "Failed to create VkInstance!");
}


void VulkanBase::pickDevice() {
	uint32_t devicesCount = 0;
	vkEnumeratePhysicalDevices(instance, &devicesCount, nullptr);
	log.check_failure(devicesCount != 0, "No Vulkan supporting devices found");

	std::vector<VkPhysicalDevice> devices(devicesCount);
	vkEnumeratePhysicalDevices(instance, &devicesCount, devices.data());

	physicalDevice = devices[0]; // TODO: select device based on features, extentions and queues support

	vkGetPhysicalDeviceProperties(physicalDevice, &physDeviceProperties);

	std::stringstream msg;
	msg << "Running on " << physDeviceProperties.deviceName;
	log.message(msg.str().c_str());

	log.check_failure(checkDeviceExtsSupport(deviceExtentions), "Some device extentions are not suuported");

	SwapchainProperties swapchainProperties = getSwapchainProperties();
	log.check_failure(!swapchainProperties.surfaceFormats.empty() && !swapchainProperties.surfacePresentModes.empty(),
		"Swapchin is inadequate");
}

SwapchainProperties VulkanBase::getSwapchainProperties() {
	SwapchainProperties properties;
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, &properties.surfaceCapabilities);
	uint32_t surfaceFormatCount = 0;
	vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &surfaceFormatCount, nullptr);
	if (surfaceFormatCount != 0) {
		properties.surfaceFormats.resize(surfaceFormatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &surfaceFormatCount, properties.surfaceFormats.data());
	}
	uint32_t presentModesCount = 0;
	vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &presentModesCount, nullptr);
	if (surfaceFormatCount != 0) {
		properties.surfacePresentModes.resize(presentModesCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, &surfaceFormatCount, properties.surfacePresentModes.data());
	}
	return properties;
}


void VulkanBase::createSwapchain(bool useOld) {
	SwapchainProperties swapchainProperties = getSwapchainProperties();
	VkSurfaceFormatKHR surfaceFormat = swapchainProperties.chooseSurfaceFormat();
	VkPresentModeKHR presentMode = swapchainProperties.choosePresentMode();
	VkExtent2D extent = swapchainProperties.chooseExtent(window);

	uint32_t imageCount = swapchainProperties.surfaceCapabilities.minImageCount + 1; // one extra not to wait for the next image
	uint32_t maxImageCount = swapchainProperties.surfaceCapabilities.maxImageCount;
	if (maxImageCount != 0 && imageCount > maxImageCount) {
		imageCount = maxImageCount;
	}

	VkSwapchainCreateInfoKHR createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface = surface;
	createInfo.minImageCount = imageCount;
	createInfo.imageFormat = surfaceFormat.format;
	createInfo.imageColorSpace = surfaceFormat.colorSpace;
	createInfo.imageExtent = extent;
	createInfo.imageArrayLayers = 1; //may be more say, for a stereoscopic image
	// use VK_IMAGE_USAGE_TRANSFER_DST_BIT if copying from intermediate image
	createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
	if (useOld) {
		createInfo.oldSwapchain = swapchain;
	}

	// Handling rare case when GRAPHICS and PRESENTATION families are not equal
	QueueFamilyIndices familyIndices = getDeviceQueueFamilyIndices();
	if (familyIndices.graphicsFamily != familyIndices.presentationFamily) {
		createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		uint32_t queueFamilyInd[2] = { familyIndices.graphicsFamily.value(), familyIndices.presentationFamily.value() };
		createInfo.pQueueFamilyIndices = queueFamilyInd;
		createInfo.queueFamilyIndexCount = 2;
	}
	else {
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	}

	createInfo.preTransform = swapchainProperties.surfaceCapabilities.currentTransform;
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR; //ignore window opacity

	createInfo.presentMode = presentMode;
	createInfo.clipped = VK_TRUE; //optimize out pixels that are not wisible on the screen

	log.check_failure(vkCreateSwapchainKHR(device, &createInfo, nullptr, &swapchain),
		"Failed to create swapchain!",
		"...swapchain was created succesfuly");

	// get image handles
	vkGetSwapchainImagesKHR(device, swapchain, &imageCount, nullptr);
	swapchainImages.resize(imageCount);
	vkGetSwapchainImagesKHR(device, swapchain, &imageCount, swapchainImages.data());

	swapchainImageFormat = surfaceFormat.format;
	swapchainExtent = extent;
}


void VulkanBase::createLogicalDevice() {
	QueueFamilyIndices qFamilyInd = getDeviceQueueFamilyIndices();

	// do not create family with same index twice
	std::set<uint32_t> distinctFamilyInd = { qFamilyInd.graphicsFamily.value(), qFamilyInd.presentationFamily.value() };
	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
	float queuePriority = 1.0f;
	for (uint32_t ind : distinctFamilyInd) {
		VkDeviceQueueCreateInfo queueCreateInfo{};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = ind;
		queueCreateInfo.queueCount = 1;
		queueCreateInfo.pQueuePriorities = &queuePriority;
		queueCreateInfos.push_back(queueCreateInfo);
	}

	VkPhysicalDeviceFeatures features{};//not used because of VkPhysicalDeviceFeatures2
	//vkGetPhysicalDeviceFeatures(device, &features);
	features.fillModeNonSolid = true;
	//TODO: specify device features support

	// Feature for descriptor arrays
	VkPhysicalDeviceDescriptorIndexingFeaturesEXT descriptorIndexing{ };
	descriptorIndexing.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES_EXT;

	VkPhysicalDeviceFeatures2 features2 = { };
	features2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
	features2.pNext = &descriptorIndexing;

	vkGetPhysicalDeviceFeatures2(physicalDevice, &features2); // enable all the features our GPU has

	VkDeviceCreateInfo createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	createInfo.pEnabledFeatures = nullptr;//&features;
	createInfo.pQueueCreateInfos = queueCreateInfos.data();
	createInfo.queueCreateInfoCount = queueCreateInfos.size();
	createInfo.ppEnabledExtensionNames = deviceExtentions.data();
	createInfo.enabledExtensionCount = deviceExtentions.size();
	createInfo.pNext = &features2;

	log.check_failure(vkCreateDevice(physicalDevice, &createInfo, nullptr, &device), "Failed to create logical device");

	vkGetDeviceQueue(device, qFamilyInd.graphicsFamily.value(), 0, &graphicsQueue);
	vkGetDeviceQueue(device, qFamilyInd.presentationFamily.value(), 0, &presentQueue);
}


QueueFamilyIndices VulkanBase::getDeviceQueueFamilyIndices() {
	uint32_t count = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &count, nullptr);
	std::vector<VkQueueFamilyProperties> families(count);
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &count, families.data());
	QueueFamilyIndices indices;
	for (int i = 0; i < families.size(); ++i) {
		// graphics
		if (families[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
			indices.graphicsFamily = i;
		}
		// presentation
		VkBool32 presentationSupport = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, surface, &presentationSupport);
		if (presentationSupport) {
			indices.presentationFamily = i;
		}

		if (indices.isComplete()) {
			break;
		}
	}
	return indices;
}

bool VulkanBase::checkSupport(const std::vector<const char*>& requested, const std::vector<const char*>& supported) {
	bool all_supported = true;
	for (const auto& rname : requested) {
		bool sup = false;
		for (const auto& sname : supported) {
			if (strcmp(sname, rname) == 0) {
				sup = true;
			}
		}
		if (!sup) {
			all_supported = false;
			std::stringstream msg;
			msg << rname << " is not supported";
			log.warning(msg.str().c_str());
		}
	}
	return all_supported;
}

bool VulkanBase::checkDeviceExtsSupport(const std::vector<const char*>& requested) {
	uint32_t extCount = 0;
	vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &extCount, nullptr);
	std::vector<VkExtensionProperties> extensions(extCount);
	vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &extCount, extensions.data());
	std::vector<const char*> supported(extCount);
	for (int i = 0; i < extCount; ++i) {
		supported[i] = extensions[i].extensionName;
	}
	return checkSupport(requested, supported);
}

bool VulkanBase::checkGlobalExtsSupport(const std::vector<const char*>& requested) {
	uint32_t supportedExtCount = 0;
	vkEnumerateInstanceExtensionProperties(nullptr, &supportedExtCount, nullptr);
	std::vector<VkExtensionProperties> extensions(supportedExtCount);
	vkEnumerateInstanceExtensionProperties(nullptr, &supportedExtCount, extensions.data());
	std::vector<const char*> supported(supportedExtCount);
	for (int i = 0; i < supportedExtCount; ++i) {
		supported[i] = extensions[i].extensionName;
	}
	return checkSupport(requested, supported);
}

bool VulkanBase::checkValidationLayersSupport() {
	uint32_t supportedLayersCount = 0;
	vkEnumerateInstanceLayerProperties(&supportedLayersCount, nullptr);
	std::vector<VkLayerProperties> layers(supportedLayersCount);
	vkEnumerateInstanceLayerProperties(&supportedLayersCount, layers.data());
	std::vector<const char*> supported(supportedLayersCount);
	for (int i = 0; i < supportedLayersCount; ++i) {
		supported[i] = layers[i].layerName;
	}
	return checkSupport(validationLayers, supported);
}

VkShaderModule VulkanBase::createShaderModule(const std::vector<char>& binary) {
	VkShaderModuleCreateInfo createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.codeSize = binary.size();
	createInfo.pCode = reinterpret_cast<const uint32_t*>(binary.data()); //32-bit alignment is ensured by std::vector allocator
	VkShaderModule shaderModule;
	log.check_failure(vkCreateShaderModule(device, &createInfo, nullptr, &shaderModule), "Failed to create shader module!");
	return shaderModule;
}

uint32_t VulkanBase::chooseMemoryType(uint32_t filter, VkMemoryPropertyFlags properties) {
	VkPhysicalDeviceMemoryProperties memoryProperties;
	vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProperties);
	//choose first suitable memory type
	for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; i++) {
		//passes filter and all requesyted properties are supported
		if ((filter & (1 << i)) && (memoryProperties.memoryTypes[i].propertyFlags & properties) == properties) {
			return i;
		}
	}
	log.failure("Failed to find an suitable memory type for the buffer!");
}

void VulkanBase::createCommandPool() {
	QueueFamilyIndices queueFamilyindices = getDeviceQueueFamilyIndices();

	VkCommandPoolCreateInfo commandPoolInfo{};
	commandPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	commandPoolInfo.queueFamilyIndex = queueFamilyindices.graphicsFamily.value();
	//TRANSIENT - frequently writing commands, RESET_COMMAND_BUFFER - multiple command buffers reset separately
	commandPoolInfo.flags = 0;
	log.check_failure(vkCreateCommandPool(device, &commandPoolInfo, nullptr, &commandPool), "Failed to create command pool!");
}

void VulkanBase::createCommandBuffers() {
	commandBuffers.resize(swapchainImages.size());

	VkCommandBufferAllocateInfo commandBufferInfo{};
	commandBufferInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferInfo.commandPool = commandPool;
	commandBufferInfo.commandBufferCount = commandBuffers.size();
	commandBufferInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY; // SECONDARY can only be called by primary
	log.check_failure(vkAllocateCommandBuffers(device, &commandBufferInfo, commandBuffers.data()),
		"Failed to allocate command buffers!");

	for (int i = 0; i < swapchainImages.size(); ++i) {
		VkCommandBufferBeginInfo beginInfo{};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		beginInfo.flags = 0; //VK_COMMAND_BUFFER_USAGE_
		beginInfo.pInheritanceInfo = nullptr; //states inherited from calling PRIMARY buffer

		VkCommandBuffer cmdBuffer = commandBuffers[i];

		log.check_failure(vkBeginCommandBuffer(cmdBuffer, &beginInfo), "Failed to start recording command buffer!");

		vkCmdResetQueryPool(cmdBuffer, timeQueryPool, i * 2, 2);

		vkCmdWriteTimestamp(cmdBuffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, timeQueryPool, i * 2);

		recordPipelineDrawCommands(cmdBuffer, i);

		vkCmdWriteTimestamp(cmdBuffer, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, timeQueryPool, i * 2 + 1);

		log.check_failure(vkEndCommandBuffer(cmdBuffer), "Failed to record command buffer!", "...command buffer recorded");
	}
}

void VulkanBase::createSyncObjects() {
	swapchainImageSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
	renderSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
	frameFences.resize(MAX_FRAMES_IN_FLIGHT);
	renderFences.resize(swapchainImages.size(), VK_NULL_HANDLE);

	VkSemaphoreCreateInfo semaphoreInfo{};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VkFenceCreateInfo fenceInfo{};
	fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
		auto msg = "Failed to create sync objects!";
		log.check_failure(vkCreateSemaphore(device, &semaphoreInfo, nullptr, &swapchainImageSemaphores[i]), msg);
		log.check_failure(vkCreateSemaphore(device, &semaphoreInfo, nullptr, &renderSemaphores[i]), msg);
		log.check_failure(vkCreateFence(device, &fenceInfo, nullptr, &frameFences[i]), msg);
	}
}


void VulkanBase::drawFrame() {
	uint32_t imageIndex;
	size_t frameInd = renderState.framesCnt % MAX_FRAMES_IN_FLIGHT;
	// wait for an image to be released by the swapchain and signal to the pipeline
	VkResult imgAqRes = vkAcquireNextImageKHR(device, swapchain, UINT64_MAX, swapchainImageSemaphores[frameInd], VK_NULL_HANDLE, &imageIndex);
	if (imgAqRes == VK_ERROR_OUT_OF_DATE_KHR || /*for the error code is not guaranteed on resize*/ windowResized) {
		onResized(imageIndex);
		return;
	}
	log.check_failure(imgAqRes == VK_SUCCESS || imgAqRes == VK_SUBOPTIMAL_KHR, "Failed to aquire swapchain image");

	// wait for an image to be released by the graphics
	if (renderFences[imageIndex] != VK_NULL_HANDLE) {
		vkWaitForFences(device, 1, &renderFences[imageIndex], VK_TRUE, UINT64_MAX);
	}
	renderFences[imageIndex] = frameFences[frameInd];

	frameUpdateBase(imageIndex);

	VkSubmitInfo submitInfo{};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	VkPipelineStageFlags pipelineStageFlags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;// VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT
	submitInfo.pWaitDstStageMask = &pipelineStageFlags;
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = &swapchainImageSemaphores[frameInd]; //wait on COLOR_ATTACHMENT_OUTPUT until swapchain releases the image
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffers[imageIndex];
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = &renderSemaphores[frameInd]; //wake presenter when image was rendered

	vkResetFences(device, 1, &frameFences[frameInd]);

	log.check_failure(
		vkQueueSubmit(graphicsQueue, 1, &submitInfo, frameFences[frameInd]),
		"Failed to submit comnnads to graphics queue!");

	VkPresentInfoKHR presentInfo{};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = &renderSemaphores[frameInd];
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = &swapchain;
	presentInfo.pImageIndices = &imageIndex;
	presentInfo.pResults = nullptr;// set to check results for multiple swapchains

	vkQueuePresentKHR(presentQueue, &presentInfo);

	renderState.framesCnt++;

	return;
}


void VulkanBase::onResized(int imageIndex) {

	while (winWidth == 0 || winHeight == 0) {
		glfwWaitEvents();
	}
	windowResized = false;

	vkDeviceWaitIdle(device);

	scene->camera->fovXYRatio = winWidth / (float)winHeight;

	cleanupSyncObjects();

	cleanupSwapchainDeps();

	vkDestroySwapchainKHR(device, swapchain, nullptr);
	vkFreeCommandBuffers(device, commandPool, static_cast<uint32_t>(commandBuffers.size()), commandBuffers.data());
	createSwapchain(false);
	createSwapchainDeps();
	createCommandBuffers();
	createSyncObjects();
}

// -------------------- helper functions

void VulkanBase::transferImageLayout(VkCommandBuffer commandBuffer,
	VkImage image,
	VkImageLayout oldLayout,
	VkImageLayout newLayout) {

	VkImageSubresourceRange subresourceRange{};
	subresourceRange.layerCount = 1;
	subresourceRange.levelCount = 1;
	subresourceRange.baseMipLevel = 0;
	subresourceRange.baseArrayLayer = 0;
	subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;

	VkImageMemoryBarrier imageMemBarrier{};
	imageMemBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	imageMemBarrier.image = image;
	imageMemBarrier.oldLayout = oldLayout;
	imageMemBarrier.newLayout = newLayout;
	imageMemBarrier.subresourceRange = subresourceRange;
	imageMemBarrier.srcAccessMask = getImageLayoutAccessFalgs(oldLayout); //actions to finish before transition
	imageMemBarrier.dstAccessMask = getImageLayoutAccessFalgs(newLayout); //what actions will depend on new layout

	vkCmdPipelineBarrier(commandBuffer,
		VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
		VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
		0,
		0, nullptr,
		0, nullptr,
		1, &imageMemBarrier);

}

void VulkanBase::createImage(int width, int height,
	VkFormat format,
	VkImageUsageFlags imageUsage,
	VkImage& image,
	VkDeviceMemory& imageMemory) {
	//Create image
	VkImageCreateInfo imageInfo{};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	//VK_FORMAT_B8G8R8A8_SRGB is not supported!
	imageInfo.format = format;//swapchainImageFormat;
	imageInfo.extent.width = width;
	imageInfo.extent.height = height;
	imageInfo.extent.depth = 1;
	imageInfo.imageType = VK_IMAGE_TYPE_2D;
	imageInfo.mipLevels = 1;
	imageInfo.arrayLayers = 1;
	imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
	imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
	imageInfo.usage = imageUsage;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	log.check_failure(vkCreateImage(device, &imageInfo, nullptr, &image),
		"Failed to create storage image");
	//Allocate image memory
	VkMemoryRequirements imageMemoryRequirements;
	vkGetImageMemoryRequirements(device, image, &imageMemoryRequirements);
	VkMemoryAllocateInfo allocInfo{};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = imageMemoryRequirements.size;
	allocInfo.memoryTypeIndex = chooseMemoryType(imageMemoryRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
	log.check_failure(vkAllocateMemory(device, &allocInfo, nullptr, &imageMemory),
		"Failed to allocate storage image memory");
	log.check_failure(vkBindImageMemory(device, image, imageMemory, 0), "Failed to bind storage image memory");
}

void VulkanBase::createTimestampQueryPool(VkQueryPool& pool) {
	log.check_failure(physDeviceProperties.limits.timestampComputeAndGraphics,
		"Timestamp queries are not supported");

	VkQueryPoolCreateInfo queryPoolInfo{};
	queryPoolInfo.sType = VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO;
	queryPoolInfo.queryType = VK_QUERY_TYPE_TIMESTAMP;
	queryPoolInfo.queryCount = 2 * swapchainImages.size();
	log.check_failure(vkCreateQueryPool(device, &queryPoolInfo, nullptr, &pool),
		"Failed to create query pool");
}

VkImageView VulkanBase::createImageView(VkImage image, VkFormat format) {

	VkImageViewCreateInfo imageViewInfo{};
	imageViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	imageViewInfo.image = image;
	imageViewInfo.format = format;
	imageViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	imageViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	imageViewInfo.subresourceRange.baseMipLevel = 0;
	imageViewInfo.subresourceRange.levelCount = 1;
	imageViewInfo.subresourceRange.baseArrayLayer = 0;
	imageViewInfo.subresourceRange.layerCount = 1;

	VkImageView imageView;
	log.check_failure(vkCreateImageView(device, &imageViewInfo, nullptr, &imageView),
		"Failed to create an image view");
	return imageView;
}

VkCommandBuffer VulkanBase::beginNewCommandBuffer() {
	VkCommandBufferAllocateInfo cmdBuffInfo{};
	cmdBuffInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	cmdBuffInfo.commandPool = commandPool;
	cmdBuffInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	cmdBuffInfo.commandBufferCount = 1;

	VkCommandBuffer commandBuffer;
	vkAllocateCommandBuffers(device, &cmdBuffInfo, &commandBuffer);

	VkCommandBufferBeginInfo beginInfo{};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	vkBeginCommandBuffer(commandBuffer, &beginInfo);

	return commandBuffer;
}

void VulkanBase::endAndFlushCommandBuffer(VkCommandBuffer commandBuffer) {
	vkEndCommandBuffer(commandBuffer);

	VkSubmitInfo submitInfo{};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;
	vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
	vkQueueWaitIdle(graphicsQueue);

}

void VulkanBase::createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags propertyFlags,
	VkBuffer& buffer, VkDeviceMemory& bufferMemory) {

	// Create buffer
	VkBufferCreateInfo bufferInfo{};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = size;
	bufferInfo.usage = usage;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE; // used by graphics queue only
	log.check_failure(vkCreateBuffer(device, &bufferInfo, nullptr, &buffer),
		"Failed to create a buffer!");

	// Allocate memory
	VkMemoryRequirements memRequirements;
	vkGetBufferMemoryRequirements(device, buffer, &memRequirements);
	VkMemoryAllocateInfo allocationInfo{};
	allocationInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocationInfo.allocationSize = std::max(memRequirements.size, bufferInfo.size);

	// use VK_MEMORY_PROPERTY_HOST_COHERENT_BIT instead uf memory flushing may lead to decreased performance
	allocationInfo.memoryTypeIndex = chooseMemoryType(memRequirements.memoryTypeBits, propertyFlags);
	log.check_failure(vkAllocateMemory(device, &allocationInfo, nullptr, &bufferMemory), "Failed to allocate buffer memory!");

	// specify offset != 0 if storing something else in the beginning of the buffer
	// offset % memRequireemtns.allignment = 0
	vkBindBufferMemory(device, buffer, bufferMemory, 0);
}

void VulkanBase::copyBuffer(VkBuffer dst, VkBuffer src, VkDeviceSize size) {
	VkCommandBufferAllocateInfo cmdBuffInfo{};
	cmdBuffInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	cmdBuffInfo.commandPool = commandPool;
	cmdBuffInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	cmdBuffInfo.commandBufferCount = 1;

	VkCommandBuffer commandBuffer;
	vkAllocateCommandBuffers(device, &cmdBuffInfo, &commandBuffer);

	VkCommandBufferBeginInfo beginInfo{};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	vkBeginCommandBuffer(commandBuffer, &beginInfo);

	VkBufferCopy copyRegion{};
	copyRegion.srcOffset = 0;
	copyRegion.dstOffset = 0;
	copyRegion.size = size;
	vkCmdCopyBuffer(commandBuffer, src, dst, 1, &copyRegion);

	vkEndCommandBuffer(commandBuffer);

	VkSubmitInfo submitInfo{};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;
	vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
	vkQueueWaitIdle(graphicsQueue);
}
