#ifndef SHADERS_SHARED
#define SHADERS_SHARED

struct Payload {
	vec3 normal;
	vec2 uv;
	float hitT;
	uint objId;
};

struct LightSource {
	vec4 pos;
	vec4 color;
	bool isDirectional;
};

struct Material {
	vec4 diffuse;
	int diffuseTexId;
	float specular;
	float sharpness;
};

struct UniformParams {
	vec3 camPos;
	vec3 camDir;
	vec3 camUp;
	vec3 camSide;
	vec4 camNearFarFovYRatio;
	int numLightSources;
	int recursionDepth;
	int numSamples;
	int sampleInd;
	bool gammaCorrect;
	bool invGammaCorrect;
	bool readBGR;
	bool writeBGR;
};

#define SAMPLES_PER_FRAME 5

#endif //SHADERS_SHARED

