#include "ray_tracer_nv.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#define __STDC_LIB_EXT1__
#include "stb_image_write.h"

VkPhysicalDeviceRayTracingPropertiesNV rayTracingProperties;

void RayTracerNV::initRayTracing() {
	rayTracingProperties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PROPERTIES_NV;
	VkPhysicalDeviceProperties2 deviceProps2{};
	deviceProps2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
	deviceProps2.pNext = &rayTracingProperties;
	vkGetPhysicalDeviceProperties2(physicalDevice, &deviceProps2);
	// set up ray tracing properties
	rtParams.recursionDepth = arguments.maxBounces;
	rtParams.numSamples = arguments.numSamples;
}

RayTracerNV::raysCountVec RayTracerNV::getSecondaryRaysCount(int swapchainIndex) {
	Buffer srcBuffer = rayCountBuffers[swapchainIndex];
	std::vector<raysCountVec> rayCounts(winWidth * winHeight);
	size_t size = rayCounts.size() * sizeof(raysCountVec);
	void* mapped;
	vkMapMemory(device, srcBuffer.memory, 0, size, 0, &mapped);
	memcpy(rayCounts.data(), mapped, size);
	vkUnmapMemory(device, srcBuffer.memory);
	return std::accumulate(rayCounts.begin(), rayCounts.end(), raysCountVec{});
}

void RayTracerNV::printFinalTimings() {
	log.message("t | r/s *10^6 | sr/sp *10^3 |");
	std::cout << std::setprecision(2) << renderState.cpuTimeMs / 1000.f
		<< " " << std::fixed << getRPS(renderState, rtTotalStats) / 1000'000.
		<< " " << (rtTotalStats.numSamples == 0 ? 0. : rtTotalStats.secRaysCount / rtTotalStats.numSamples / 1000.)
		<< std::endl;
}

void RayTracerNV::frameUpdate(int swapchainIndex) {
	fillUniformBuffer();
	int frameTime = getGPUFrameTimeMs(rtTimeQueryPool, swapchainIndex);
	if (frameTime != -1) {
		rtTotalStats.traceRaysTimeMs += frameTime;
	}
	// Counting rays is expensive and should be done only after the image is updated
	if (sceneUpdated) {
		updatedImageIndex = swapchainIndex;
	}
	else {
		if (swapchainIndex == updatedImageIndex && arguments.countRays) {
			raysCountVec rc = getSecondaryRaysCount(swapchainIndex) / rtParams.numSamples;
			curRaysPerSample = rc.x;
			curShadowRaysPerSample = rc.y;
			updatedImageIndex = -1;
		}
	}
	if (curRaysPerSample != 0 && rtParams.sampleInd < rtParams.numSamples) {
		rtTotalStats.secRaysCount += curRaysPerSample * rtParams.numSamples;
		rtTotalStats.shadowRaysCount += curShadowRaysPerSample * rtParams.numSamples;
		rtTotalStats.numSamples += rtParams.numSamples;
	}
	static bool saved = false;
	if (rtParams.sampleInd < rtParams.numSamples) {
		rtParams.sampleInd += std::min(int(rtParams.numSamples - rtParams.sampleInd), SAMPLES_PER_FRAME);
		saved = false;
	}
	else {
		//render finished
		if (arguments.imagePath != "" && !saved) {
			saveImage();
			saved = true;
		}
		if (arguments.offscreen) {
			renderTerminated = true;
		}
	}
	if (sceneUpdated) {
		rtParams.sampleInd = 0;
	}
}

std::string extension(std::string path) {
	std::string str;
	for (char c : path) {
		str = str + c;
		if (c == '.') {
			str = "";
		}
	}
	tolower(str);
	return str;
}

void writeImage(std::string path, const void* data, int w, int h) {

	std::string ext = extension(path);
	if (ext == "png") {
		stbi_write_png(path.data(), w, h, 4, data, w * 4);
	}
	else if (ext == "jpg" || ext == "jpeg") {
		stbi_write_jpg(path.data(), w, h, 4, data, 100);
	}
	else if (ext == "bmp") {
		stbi_write_bmp(path.data(), w, h, 4, data);
	}
	else if (ext == "tga") {
		stbi_write_tga(path.data(), w, h, 4, data);
	}
	else if (ext == "hdr") {
		stbi_write_hdr(path.data(), w, h, 4, (const float*)data);
	}
	
}

void RayTracerNV::saveImage() {
	Buffer buf;
	VkDeviceSize size = winWidth * winHeight * 4;
	createBuffer(size, VK_BUFFER_USAGE_TRANSFER_DST_BIT,
		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_CACHED_BIT,
		buf.buffer, buf.memory);
	VkBufferImageCopy imageCopy{};
	imageCopy.bufferImageHeight = winHeight;
	imageCopy.bufferRowLength = winWidth;
	imageCopy.bufferOffset = 0;
	imageCopy.imageExtent = { winWidth, winHeight, 1 };
	imageCopy.imageOffset = { 0, 0, 0 };
	imageCopy.imageSubresource = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1 };

	vkQueueWaitIdle(graphicsQueue);

	bool overlay = !rtParams.gammaCorrect; // detect whether there is an overlay
	
	if (overlay) {
		rtParams.gammaCorrect = true;
		fillUniformBuffer();
		rtParams.gammaCorrect = false;
	}
	else {
		rtParams.writeBGR = false;
		fillUniformBuffer();
		rtParams.writeBGR = true;
	}
	
	VkCommandBuffer cmd = beginNewCommandBuffer();
	vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_RAY_TRACING_NV, rtPipeline);
	vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_RAY_TRACING_NV, rtLayout, 0, 1,
		&rtDescriptorSet, 0, 0);
	traceRays(cmd);
	transferImageLayout(cmd, rtImage.image, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
	vkCmdCopyImageToBuffer(cmd, rtImage.image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, buf.buffer, 1, &imageCopy);
	transferImageLayout(cmd, rtImage.image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL);
	endAndFlushCommandBuffer(cmd);
	
	void* data;
	vkMapMemory(device, buf.memory, 0, size, 0, &data);
	
	writeImage(arguments.imagePath, data, winWidth, winHeight);

	vkUnmapMemory(device, buf.memory);
	buf.destroy(device);

	if (overlay) {
		rtParams.invGammaCorrect = true;
		fillUniformBuffer();
		rtParams.invGammaCorrect = false;
	}
	else {
		rtParams.readBGR = false;
		fillUniformBuffer();
		rtParams.readBGR = true;
	}

	cmd = beginNewCommandBuffer();
	vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_RAY_TRACING_NV, rtPipeline);
	vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_RAY_TRACING_NV, rtLayout, 0, 1,
		&rtDescriptorSet, 0, 0);
	traceRays(cmd);
	endAndFlushCommandBuffer(cmd);
		
	fillUniformBuffer();
}

void RayTracerNV::updateMaxSamples(int key, int action) {
	static int inpNumber = 0;
	static bool pressed[10] = {};
	for (int i = 0; i < 10; ++i) {
		if (key == GLFW_KEY_0 + i) {
			if (action == GLFW_PRESS && !pressed[i]) {
				inpNumber *= 10;
				inpNumber += i;
				pressed[i] = true;
			}
			if (action == GLFW_RELEASE) {
				pressed[i] = false;
			}
		}
		if (action == GLFW_PRESS && key == GLFW_KEY_ENTER) {
			if (inpNumber != 0) {
				rtParams.sampleInd = 0;
				rtParams.numSamples = inpNumber;
				sceneUpdated = true;
			}
			inpNumber = 0;
		}
	}
}

void RayTracerNV::updateRecursionDepth(int key, int action) {
	static bool inc = false;
	static bool dec = false;

	if (action == GLFW_PRESS && key == GLFW_KEY_UP && !inc) {
		rtParams.recursionDepth++;
		inc = true;
		sceneUpdated = true;
	}
	if (action == GLFW_RELEASE && key == GLFW_KEY_UP) {
		inc = false;
	}
	if (action == GLFW_PRESS && key == GLFW_KEY_DOWN
		&& rtParams.recursionDepth > 1 && !dec) {

		rtParams.recursionDepth--;
		dec = true;
		sceneUpdated = true;
	}
	if (action == GLFW_RELEASE && key == GLFW_KEY_DOWN) {
		dec = false;
	}
}


void RayTracerNV::createAccelerationStructures() {
	auto vkCreateAccelerationStructureNV = PFN_vkCreateAccelerationStructureNV(vkGetDeviceProcAddr(device, "vkCreateAccelerationStructureNV"));

	auto vkGetAccelerationStructureMemoryRequirementsNV = PFN_vkGetAccelerationStructureMemoryRequirementsNV(vkGetDeviceProcAddr(device, "vkGetAccelerationStructureMemoryRequirementsNV"));
	auto vkBindAccelerationStructureMemoryNV = PFN_vkBindAccelerationStructureMemoryNV(vkGetDeviceProcAddr(device, "vkBindAccelerationStructureMemoryNV"));
	for (const auto& ptr : scene->sceneObjects) {
		BLAS blas;

		blas.sceneObject = static_cast<RTSceneObject*>(ptr.get());
		RTSceneObject& ro = *blas.sceneObject;

		VkGeometryTrianglesNV triangles{};
		triangles.sType = VK_STRUCTURE_TYPE_GEOMETRY_TRIANGLES_NV;
		//indices
		triangles.indexCount = ro.indexBuffer.count;
		triangles.indexData = ro.indexBuffer.buffer;
		triangles.indexType = VK_INDEX_TYPE_UINT32;
		triangles.indexOffset = 0;
		//vertices
		triangles.vertexCount = ro.vertexBuffer.count;
		triangles.vertexData = ro.vertexBuffer.buffer;
		triangles.vertexFormat = VK_FORMAT_R32G32B32_SFLOAT;
		triangles.vertexStride = sizeof(Vertex);
		triangles.vertexOffset = 0;

		VkGeometryAABBNV aabbs{};
		aabbs.sType = VK_STRUCTURE_TYPE_GEOMETRY_AABB_NV;

		VkGeometryDataNV geometryData{};
		geometryData.triangles = triangles;
		geometryData.aabbs = aabbs;

		VkGeometryNV geometry{};
		geometry.sType = VK_STRUCTURE_TYPE_GEOMETRY_NV;
		geometry.geometry = geometryData;
		geometry.geometryType = VK_GEOMETRY_TYPE_TRIANGLES_NV;
		geometry.flags = VK_GEOMETRY_OPAQUE_BIT_NV;

		blas.geometry = std::make_unique<VkGeometryNV>(geometry);

		//creating BLAS
		VkAccelerationStructureInfoNV blasInfo{};
		blasInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_INFO_NV;
		blasInfo.geometryCount = 1;
		blasInfo.pGeometries = blas.geometry.get();
		blasInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_NV;
		blasInfo.flags = VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_COMPACTION_BIT_NV;

		VkAccelerationStructureCreateInfoNV blasCreateInfo{};
		blasCreateInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_NV;
		blasCreateInfo.info = blasInfo;

		blas.info = blasInfo;

		log.check_failure(vkCreateAccelerationStructureNV(device, &blasCreateInfo, nullptr, &blas.blas),
			"Failed to create BLAS",
			"BLAS created");

		// Allocating BLAS memory
		VkAccelerationStructureMemoryRequirementsInfoNV blasMemReqInfo{};
		blasMemReqInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_INFO_NV;
		blasMemReqInfo.accelerationStructure = blas.blas;
		blasMemReqInfo.type = VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_OBJECT_NV;

		vkGetAccelerationStructureMemoryRequirementsNV(device, &blasMemReqInfo, &blas.memReq);

		VkMemoryAllocateInfo blasAllocInfo{};
		blasAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		blasAllocInfo.allocationSize = blas.memReq.memoryRequirements.size;
		blasAllocInfo.memoryTypeIndex = chooseMemoryType(blas.memReq.memoryRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

		log.check_failure(vkAllocateMemory(device, &blasAllocInfo, nullptr, &blas.memory),
			"Failed to allocate BLAS memory",
			"BLAS memory allocated");

		VkBindAccelerationStructureMemoryInfoNV blasBindMemInfo{};
		blasBindMemInfo.sType = VK_STRUCTURE_TYPE_BIND_ACCELERATION_STRUCTURE_MEMORY_INFO_NV;
		blasBindMemInfo.accelerationStructure = blas.blas;
		blasBindMemInfo.memory = blas.memory;

		log.check_failure(vkBindAccelerationStructureMemoryNV(device, 1, &blasBindMemInfo),
			"Failed to bind BLAS memory",
			"BLAS memory bound");

		auto vkGetAccelerationStructureHandleNV = PFN_vkGetAccelerationStructureHandleNV(vkGetDeviceProcAddr(device, "vkGetAccelerationStructureHandleNV"));
		vkGetAccelerationStructureHandleNV(device, blas.blas, sizeof(uint64_t), &blas.handle);

		blases.push_back(blas);
	}

	std::vector<GeometryInstance> geometryInstances;
	int id = 0;
	for (auto& data : blases) {
		for (auto& node : data.sceneObject->nodes) {
			GeometryInstance instance{};
			instance.transform = glm::mat3x4(glm::transpose(node->transform));
			instance.instanceId = id;
			instance.mask = 0xff;
			instance.instanceOffset = 0;
			instance.flags = VK_GEOMETRY_INSTANCE_TRIANGLE_CULL_DISABLE_BIT_NV;
			instance.accelerationStructureHandle = data.handle;

			geometryInstances.push_back(instance);
		}
		id++;
	}
	//Creating TLAS
	VkAccelerationStructureInfoNV tlasInfo{};
	tlasInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_INFO_NV;
	tlasInfo.instanceCount = geometryInstances.size();
	tlasInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_NV;

	VkAccelerationStructureCreateInfoNV tlasCreateInfo{};
	tlasCreateInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_NV;
	tlasCreateInfo.info = tlasInfo;

	log.check_failure(vkCreateAccelerationStructureNV(device, &tlasCreateInfo, nullptr, &tlas),
		"Failed to create TLAS",
		"TLAS created");

	// Allocating TLAS memory
	VkAccelerationStructureMemoryRequirementsInfoNV tlasMemReqInfo{};
	tlasMemReqInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_INFO_NV;
	tlasMemReqInfo.accelerationStructure = tlas;

	VkMemoryRequirements2KHR tlasMemReq;
	vkGetAccelerationStructureMemoryRequirementsNV(device, &tlasMemReqInfo, &tlasMemReq);

	tlasSize = tlasMemReq.memoryRequirements.size;

	VkMemoryAllocateInfo tlasAllocInfo{};
	tlasAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	tlasAllocInfo.allocationSize = tlasMemReq.memoryRequirements.size;
	tlasAllocInfo.memoryTypeIndex = chooseMemoryType(tlasMemReq.memoryRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	log.check_failure(vkAllocateMemory(device, &tlasAllocInfo, nullptr, &tlasMem),
		"Failed to allocate TLAS memory",
		"TLAS memory allocated");

	VkBindAccelerationStructureMemoryInfoNV tlasBindMemInfo{};
	tlasBindMemInfo.sType = VK_STRUCTURE_TYPE_BIND_ACCELERATION_STRUCTURE_MEMORY_INFO_NV;
	tlasBindMemInfo.accelerationStructure = tlas;
	tlasBindMemInfo.memory = tlasMem;

	log.check_failure(vkBindAccelerationStructureMemoryNV(device, 1, &tlasBindMemInfo),
		"Failed to bind TLAS memory",
		"TLAS memory bound");

	//Buffer with geometry instance

	size_t instancesSize = sizeof(GeometryInstance) * geometryInstances.size();
	createBuffer(instancesSize, VK_BUFFER_USAGE_RAY_TRACING_BIT_NV,
		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
		geomInstanceBuffer.buffer, geomInstanceBuffer.memory);
	void* mapped;
	log.check_failure(vkMapMemory(device, geomInstanceBuffer.memory, 0, instancesSize, 0, &mapped), 
		"Failed to map GeometryInstance buffer memory");
	memcpy(mapped, geometryInstances.data(), instancesSize);
	vkUnmapMemory(device, geomInstanceBuffer.memory);

	VkQueryPoolCreateInfo asSizeQueryPoolInfo{};
	asSizeQueryPoolInfo.sType = VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO;
	asSizeQueryPoolInfo.queryType = VK_QUERY_TYPE_ACCELERATION_STRUCTURE_COMPACTED_SIZE_NV;
	asSizeQueryPoolInfo.queryCount = blases.size();

	VkQueryPool asSizeQueryPool;
	log.check_failure(vkCreateQueryPool(device, &asSizeQueryPoolInfo, nullptr, &asSizeQueryPool),
		"Failed to create AS size");

	//Building BLAS & TLAS

	size_t blasMaxSize = 0;
	for (auto& blas : blases) {
		float s = blas.memReq.memoryRequirements.size / 1024.f;
		if (blas.memReq.memoryRequirements.size > blasMaxSize) {
			blasMaxSize = blas.memReq.memoryRequirements.size;
		}
	}

	VkDeviceSize scratchBufferSize = std::max(blasMaxSize, tlasMemReq.memoryRequirements.size);
	Buffer scratchBuffer;
	createBuffer(scratchBufferSize, VK_BUFFER_USAGE_RAY_TRACING_BIT_NV, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
		scratchBuffer.buffer, scratchBuffer.memory);

	auto buildASCmd = beginNewCommandBuffer();

	auto vkCmdBuildAccelerationStructureNV = PFN_vkCmdBuildAccelerationStructureNV(vkGetDeviceProcAddr(device, "vkCmdBuildAccelerationStructureNV"));

	//Build BLAS

	for (size_t i = 0; i < blases.size(); ++i) {
		auto& blas = blases[i];

		vkCmdBeginQuery(buildASCmd, asSizeQueryPool, i, 0);

		vkCmdBuildAccelerationStructureNV(buildASCmd, &blas.info, 
			VK_NULL_HANDLE, 0, VK_FALSE, blas.blas, VK_NULL_HANDLE, scratchBuffer.buffer, 0);

		vkCmdEndQuery(buildASCmd, asSizeQueryPool, i);

		VkMemoryBarrier barier{};
		barier.sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER;
		barier.srcAccessMask = VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_NV | VK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_NV;
		barier.dstAccessMask = VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_NV | VK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_NV;
		vkCmdPipelineBarrier(buildASCmd, VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_NV,
			VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_NV, 0, 1, &barier, 0, 0, 0, 0);
	}

	//Build TLAS
	vkCmdBuildAccelerationStructureNV(buildASCmd, &tlasInfo, geomInstanceBuffer.buffer, 0, VK_FALSE, tlas, VK_NULL_HANDLE, scratchBuffer.buffer, 0);

	endAndFlushCommandBuffer(buildASCmd);

	scratchBuffer.destroy(device);

	compactBlasses(asSizeQueryPool);

	vkDestroyQueryPool(device, asSizeQueryPool, nullptr);
}

void RayTracerNV::compactBlasses(VkQueryPool blasSizesQueryPool) {
	//TODO read AS compacted sizes and copy into new structures of smaller size
}

uint32_t RayTracerNV::getTexturesCount() {
	uint32_t numTextures = 0;
	for (const auto& ro : scene->sceneObjects) {
		if (static_cast<RTSceneObject*>(ro.get())->textureImage.get() != nullptr) {
			numTextures++;
		}
	}
	return numTextures;
}

void RayTracerNV::createRayTracingPipeline() {
	auto genBinary = readBinary("shaders/gen.spv");
	auto chitBinary = readBinary("shaders/chit.spv");
	auto missBinary = readBinary("shaders/miss.spv");

	raygenShaderMoodule = createShaderModule(genBinary);
	chitShaderMoodule = createShaderModule(chitBinary);
	missShaderMoodule = createShaderModule(missBinary);

	VkPipelineShaderStageCreateInfo raygenStageInfo{};
	raygenStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	raygenStageInfo.stage = VK_SHADER_STAGE_RAYGEN_BIT_NV;
	raygenStageInfo.module = raygenShaderMoodule;
	raygenStageInfo.pName = "main";

	VkPipelineShaderStageCreateInfo chitStageInfo{};
	chitStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	chitStageInfo.stage = VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV;
	chitStageInfo.module = chitShaderMoodule;
	chitStageInfo.pName = "main";

	VkPipelineShaderStageCreateInfo missStageInfo{};
	missStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	missStageInfo.stage = VK_SHADER_STAGE_MISS_BIT_NV;
	missStageInfo.module = missShaderMoodule;
	missStageInfo.pName = "main";

	std::array<VkPipelineShaderStageCreateInfo, 3> shaderStageInfos{
		raygenStageInfo, chitStageInfo, missStageInfo
	};

	uint32_t raygenShaderIndex = 0;
	uint32_t chitShaderIndex = 1;
	uint32_t missShaderIndex = 2;

	int raygenGroupIndex = 0;
	int missGroupIndex = 1;
	int hitGroupIndex = 2;

	std::array<VkRayTracingShaderGroupCreateInfoNV, 3> groups{};
	for (auto& group : groups) {
		group.sType = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_NV;
		group.closestHitShader = VK_SHADER_UNUSED_NV;
		group.generalShader = VK_SHADER_UNUSED_NV;
		group.anyHitShader = VK_SHADER_UNUSED_NV;
		group.intersectionShader = VK_SHADER_UNUSED_NV;
	}

	groups[raygenGroupIndex].type = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_NV;
	groups[raygenGroupIndex].generalShader = raygenShaderIndex;
	groups[missGroupIndex].type = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_NV;
	groups[missGroupIndex].generalShader = missShaderIndex;
	groups[hitGroupIndex].type = VK_RAY_TRACING_SHADER_GROUP_TYPE_TRIANGLES_HIT_GROUP_NV;
	groups[hitGroupIndex].closestHitShader = chitShaderIndex;

	VkDescriptorSetLayoutBinding tlasLayoutBinding{};
	tlasLayoutBinding.binding = 0;
	tlasLayoutBinding.descriptorCount = 1;
	tlasLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_NV;
	tlasLayoutBinding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_NV;

	VkDescriptorSetLayoutBinding imageLayoutBinding{};
	imageLayoutBinding.binding = 1;
	imageLayoutBinding.descriptorCount = 1;
	imageLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
	imageLayoutBinding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_NV;

	VkDescriptorSetLayoutBinding uniformsLayoutBinding{};
	uniformsLayoutBinding.binding = 2;
	uniformsLayoutBinding.descriptorCount = 1;
	uniformsLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	uniformsLayoutBinding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_NV;

	VkDescriptorSetLayoutBinding vertexAttribLayoutBinding{};
	vertexAttribLayoutBinding.binding = 3;
	vertexAttribLayoutBinding.descriptorCount = scene->sceneObjects.size();
	vertexAttribLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	vertexAttribLayoutBinding.stageFlags = VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV;

	VkDescriptorSetLayoutBinding indicesLayoutBinding{};
	indicesLayoutBinding.binding = 4;
	indicesLayoutBinding.descriptorCount = scene->sceneObjects.size();
	indicesLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	indicesLayoutBinding.stageFlags = VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV;

	VkDescriptorSetLayoutBinding materialsLayoutBinding{};
	materialsLayoutBinding.binding = 5;
	materialsLayoutBinding.descriptorCount = scene->sceneObjects.size();
	materialsLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	materialsLayoutBinding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_NV;

	VkDescriptorSetLayoutBinding texturesLayoutBinding{};
	texturesLayoutBinding.binding = 6;
	texturesLayoutBinding.descriptorCount = getTexturesCount();
	texturesLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	texturesLayoutBinding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_NV;

	VkDescriptorSetLayoutBinding lightsLayoutBinding{};
	lightsLayoutBinding.binding = 7;
	lightsLayoutBinding.descriptorCount = scene->lightSources.size();
	lightsLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	lightsLayoutBinding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_NV;

	VkDescriptorSetLayoutBinding envProbeLayoutBinding{};
	envProbeLayoutBinding.binding = 8;
	envProbeLayoutBinding.descriptorCount = 1;
	envProbeLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	envProbeLayoutBinding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_NV;

	VkDescriptorSetLayoutBinding rayCountImageBinding{};
	rayCountImageBinding.binding = 9;
	rayCountImageBinding.descriptorCount = 1;
	rayCountImageBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
	rayCountImageBinding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_NV;

	VkDescriptorSetLayoutBinding accImageBinding{};
	accImageBinding.binding = 10;
	accImageBinding.descriptorCount = 1;
	accImageBinding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
	accImageBinding.stageFlags = VK_SHADER_STAGE_RAYGEN_BIT_NV;


	std::vector<VkDescriptorSetLayoutBinding> descriptorBindings{
		tlasLayoutBinding,
		imageLayoutBinding,
		uniformsLayoutBinding,
		vertexAttribLayoutBinding,
		indicesLayoutBinding,
		materialsLayoutBinding,
		texturesLayoutBinding,
		lightsLayoutBinding,
		envProbeLayoutBinding,
		rayCountImageBinding,
		accImageBinding
	};

	VkDescriptorSetLayoutCreateInfo dsLayoutCreateInfo{};
	dsLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	dsLayoutCreateInfo.bindingCount = descriptorBindings.size();
	dsLayoutCreateInfo.pBindings = descriptorBindings.data();

	log.check_failure(vkCreateDescriptorSetLayout(device, &dsLayoutCreateInfo, nullptr, &rtDescriptorSetLayout),
		"Failed to create ray tracing descriptor set layout");

	VkPipelineLayoutCreateInfo layoutCreateInfo{};
	layoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	layoutCreateInfo.setLayoutCount = 1;
	layoutCreateInfo.pSetLayouts = &rtDescriptorSetLayout;

	log.check_failure(vkCreatePipelineLayout(device, &layoutCreateInfo, nullptr, &rtLayout),
		"Failed to create ray tracing pipeline layout");

	VkRayTracingPipelineCreateInfoNV createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_NV;
	createInfo.groupCount = groups.size();
	createInfo.pGroups = groups.data();
	createInfo.stageCount = shaderStageInfos.size();
	createInfo.pStages = shaderStageInfos.data();
	createInfo.maxRecursionDepth = rayTracingProperties.maxRecursionDepth;
	createInfo.layout = rtLayout;

	auto vkCreateRayTracingPipelinesNV = PFN_vkCreateRayTracingPipelinesNV(vkGetDeviceProcAddr(device, "vkCreateRayTracingPipelinesNV"));
	log.check_failure(vkCreateRayTracingPipelinesNV(device, VK_NULL_HANDLE, 1, &createInfo, nullptr, &rtPipeline),
		"Failed to create ray tracing pipeline",
		"Ray tracing pipeline created");
}

void RayTracerNV::createRTShaderBindingTable() {
	uint32_t size = 3/*groups number*/ * rayTracingProperties.shaderGroupHandleSize;
	createBuffer(size, VK_BUFFER_USAGE_RAY_TRACING_BIT_NV, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, sbtBuffer.buffer, sbtBuffer.memory);
	void* mapped;
	log.check_failure(vkMapMemory(device, sbtBuffer.memory, 0, size, 0, &mapped), "Failed to map SBT memory");
	
	auto vkGetRayTracingShaderGroupHandlesNV = PFN_vkGetRayTracingShaderGroupHandlesNV(vkGetDeviceProcAddr(device, "vkGetRayTracingShaderGroupHandlesNV"));
	log.check_failure(vkGetRayTracingShaderGroupHandlesNV(device, rtPipeline, 0, 3, size, mapped),
		"Failed to get shader group handles");
	
	vkUnmapMemory(device, sbtBuffer.memory);
	log.message("SBT built");
}

void RayTracerNV::createRTImage() {
	//VK_FORMAT_B8G8R8A8_SRGB (swapchainImageFormat) is not supported!
	createImage(winWidth, winHeight,
		VK_FORMAT_R8G8B8A8_UNORM,
		VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_STORAGE_BIT
		| VK_IMAGE_USAGE_SAMPLED_BIT, // used as texture in rasterization pipeline
		rtImage.image, rtImage.memory);

	rtImage.view = createImageView(rtImage.image, VK_FORMAT_R8G8B8A8_UNORM);

	//Convert image to VK_IMAGE_LAYOUT_GENERAL layout (can only be initialized with VK_IMAGE_LAYOUT_UNDEFINED
	VkCommandBuffer commandBuffer = beginNewCommandBuffer();

	transferImageLayout(commandBuffer, rtImage.image, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL);

	endAndFlushCommandBuffer(commandBuffer);

}

void RayTracerNV::createRayCountImage() {
	createImage(winWidth, winHeight, VK_FORMAT_R32G32_UINT,
		VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_STORAGE_BIT, rayCountImage.image, rayCountImage.memory);

	rayCountImage.view = createImageView(rayCountImage.image, VK_FORMAT_R32G32_UINT);

	VkCommandBuffer commandBuffer = beginNewCommandBuffer();
	transferImageLayout(commandBuffer, rayCountImage.image, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL);
	endAndFlushCommandBuffer(commandBuffer);
}

void RayTracerNV::createSeedImage() {
	createImage(winWidth, winHeight,
		VK_FORMAT_R32G32_SFLOAT,
		VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_STORAGE_BIT, // used as texture in rasterization pipeline
		seedImage.image, seedImage.memory);

	seedImage.view = createImageView(seedImage.image, VK_FORMAT_R32G32_SFLOAT);

	//Convert image to VK_IMAGE_LAYOUT_GENERAL layout (can only be initialized with VK_IMAGE_LAYOUT_UNDEFINED
	VkCommandBuffer commandBuffer = beginNewCommandBuffer();

	transferImageLayout(commandBuffer, seedImage.image, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL);

	endAndFlushCommandBuffer(commandBuffer);
}

void RayTracerNV::createUniformBuffer() {
	createBuffer(sizeof(UniformParams), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
		VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_CACHED_BIT, uniformBuffer.buffer, uniformBuffer.memory);
}

void RayTracerNV::createMaterialsBuffers() {
	uint32_t textureId = 0;
	for (const auto& so : scene->sceneObjects) {
		RTSceneObject& ro = *static_cast<RTSceneObject*>(so.get());
		Buffer buffer;
		createBuffer(sizeof(RTMaterial), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_CACHED_BIT, buffer.buffer, buffer.memory);
		RTMaterial material = ro.material;
		if (ro.textureImage.get() != nullptr) {
			material.diffuseTexId = textureId;
			textureId++;
		}
		void* data;
		vkMapMemory(device, buffer.memory, 0, sizeof(RTMaterial), 0, &data);
		memcpy(data, &material, sizeof(RTMaterial));
		vkUnmapMemory(device, buffer.memory);
		materialsBuffers.push_back(buffer);
	}
}

void RayTracerNV::createLightSourceBuffers() {
	for (const auto& light : scene->lightSources) {
		for (const auto& node : light.nodes) {
			Buffer buffer;
			createBuffer(sizeof(RTLightSource), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_CACHED_BIT, buffer.buffer, buffer.memory);
			RTLightSource rtLight;
			rtLight.pos = node->transform[3];
			rtLight.color = glm::vec4(light.color.r, light.color.g, light.color.b, 1.0f);
			rtLight.isDirectional = light.isDirectional;
			void* data;
			vkMapMemory(device, buffer.memory, 0, sizeof(RTLightSource), 0, &data);
			memcpy(data, &rtLight, sizeof(RTLightSource));
			vkUnmapMemory(device, buffer.memory);
			lightSourcesBuffers.push_back(buffer);
		}
	}
}

void RayTracerNV::createRTDescriptorSets() {
	VkDescriptorPoolSize tlasDPSize;
	tlasDPSize.type = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_NV;
	tlasDPSize.descriptorCount = 1;
	VkDescriptorPoolSize imageDPSize;
	imageDPSize.type = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
	imageDPSize.descriptorCount = 2;
	VkDescriptorPoolSize uniformsDPSize{};
	uniformsDPSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	uniformsDPSize.descriptorCount = 1 + scene->sceneObjects.size() + scene->lightSources.size();
	VkDescriptorPoolSize ssboSize{};
	ssboSize.type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	ssboSize.descriptorCount = scene->sceneObjects.size() * 2;
	VkDescriptorPoolSize texturesSize{};
	texturesSize.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	texturesSize.descriptorCount = getTexturesCount() + 1;

	std::vector<VkDescriptorPoolSize> dpSizes{ tlasDPSize, imageDPSize, uniformsDPSize, ssboSize };

	VkDescriptorPoolCreateInfo createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	createInfo.poolSizeCount = dpSizes.size();
	createInfo.pPoolSizes = dpSizes.data();
	createInfo.maxSets = 1;

	log.check_failure(vkCreateDescriptorPool(device, &createInfo, nullptr, &descriptorPool),
		"Failed to create RT descriptor pool");

	VkDescriptorSetAllocateInfo allocInfo{};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = descriptorPool;
	allocInfo.descriptorSetCount = 1;
	std::vector<VkDescriptorSetLayout> layouts(1, rtDescriptorSetLayout);
	allocInfo.pSetLayouts = layouts.data();

	log.check_failure(vkAllocateDescriptorSets(device, &allocInfo, &rtDescriptorSet),
		"Failed to allocate RT descriptor sets");

	VkWriteDescriptorSetAccelerationStructureNV descriptorACInfo{};
	descriptorACInfo.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_NV;
	descriptorACInfo.accelerationStructureCount = 1;
	descriptorACInfo.pAccelerationStructures = &tlas;

	VkWriteDescriptorSet writeAC{};
	writeAC.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeAC.descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_NV;
	writeAC.dstSet = rtDescriptorSet;
	writeAC.dstBinding = 0;
	writeAC.descriptorCount = 1;
	writeAC.pNext = &descriptorACInfo;

	VkDescriptorImageInfo descriptorImageInfo{};
	descriptorImageInfo.imageView = rtImage.view;
	descriptorImageInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
	VkWriteDescriptorSet writeImage = getImageDescriptorWrite(descriptorImageInfo, 1);

	VkDescriptorImageInfo descriptorRayCount{};
	descriptorRayCount.imageView = rayCountImage.view;
	descriptorRayCount.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
	VkWriteDescriptorSet writeRayCntImage = getImageDescriptorWrite(descriptorRayCount, 9);

	VkDescriptorImageInfo descriptorSeedImage{};
	descriptorSeedImage.imageView = seedImage.view;
	descriptorSeedImage.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
	VkWriteDescriptorSet writeSeedImage = getImageDescriptorWrite(descriptorSeedImage, 10);

	VkDescriptorBufferInfo descriptorUniformsInfo{};
	descriptorUniformsInfo.buffer = uniformBuffer.buffer;
	descriptorUniformsInfo.range = sizeof(UniformParams);

	VkWriteDescriptorSet writeUniforms{};
	writeUniforms.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeUniforms.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	writeUniforms.dstSet = rtDescriptorSet;
	writeUniforms.dstBinding = 2;
	writeUniforms.descriptorCount = 1;
	writeUniforms.pBufferInfo = &descriptorUniformsInfo;

	std::vector<VkDescriptorBufferInfo> attribBuffersInfos;
	std::vector<VkDescriptorBufferInfo> indexBuffersInfos;
	for (auto& so : scene->sceneObjects) {
		RTSceneObject& ro = *static_cast<RTSceneObject*>(so.get());

		VkDescriptorBufferInfo vertexAttribInfo{};
		vertexAttribInfo.buffer = ro.vertexBuffer.buffer;
		vertexAttribInfo.range = VK_WHOLE_SIZE;

		VkDescriptorBufferInfo indicesInfo{};
		indicesInfo.buffer = ro.indexBuffer.buffer;
		indicesInfo.range = VK_WHOLE_SIZE;

		attribBuffersInfos.push_back(vertexAttribInfo);
		indexBuffersInfos.push_back(indicesInfo);
	}

	VkWriteDescriptorSet writeVertexAttrib{};
	writeVertexAttrib.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeVertexAttrib.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	writeVertexAttrib.dstSet = rtDescriptorSet;
	writeVertexAttrib.dstBinding = 3;
	writeVertexAttrib.descriptorCount = attribBuffersInfos.size();
	writeVertexAttrib.pBufferInfo = attribBuffersInfos.data();

	VkWriteDescriptorSet writeIndices{};
	writeIndices.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeIndices.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	writeIndices.dstSet = rtDescriptorSet;
	writeIndices.dstBinding = 4;
	writeIndices.descriptorCount = indexBuffersInfos.size();
	writeIndices.pBufferInfo = indexBuffersInfos.data();

	std::vector<VkDescriptorBufferInfo> materialBuffersInfos;
	for (const auto& buffer : materialsBuffers) {
		VkDescriptorBufferInfo materialsBufferInfo{};
		materialsBufferInfo.buffer = buffer.buffer;
		materialsBufferInfo.range = VK_WHOLE_SIZE;
		materialBuffersInfos.push_back(materialsBufferInfo);
	}

	VkWriteDescriptorSet writeMaterials{};
	writeMaterials.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeMaterials.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	writeMaterials.dstSet = rtDescriptorSet;
	writeMaterials.dstBinding = 5;
	writeMaterials.descriptorCount = scene->sceneObjects.size();
	writeMaterials.pBufferInfo = materialBuffersInfos.data();

	std::vector<VkDescriptorImageInfo> textureInfos{};
	for (auto& so : scene->sceneObjects) {
		RTSceneObject& ro = *static_cast<RTSceneObject*>(so.get());
		if (ro.textureImage.get() == nullptr) {
			continue;
		}
		VkDescriptorImageInfo imageInfo{};
		imageInfo.sampler = ro.textureImage->sampler;
		imageInfo.imageView = ro.textureImage->view;
		imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		textureInfos.push_back(imageInfo);
	}

	VkWriteDescriptorSet writeTextures{};
	writeTextures.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeTextures.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	writeTextures.dstSet = rtDescriptorSet;
	writeTextures.dstBinding = 6;
	writeTextures.descriptorCount = textureInfos.size();
	writeTextures.pImageInfo = textureInfos.data();

	std::vector<VkDescriptorBufferInfo> lightsBufferInfos;
	for (const auto& buffer : lightSourcesBuffers) {
		VkDescriptorBufferInfo bufferInfo{};
		bufferInfo.buffer = buffer.buffer;
		bufferInfo.range = VK_WHOLE_SIZE;
		lightsBufferInfos.push_back(bufferInfo);
	}

	VkWriteDescriptorSet writeLightSources{};
	writeLightSources.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeLightSources.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	writeLightSources.dstSet = rtDescriptorSet;
	writeLightSources.dstBinding = 7;
	writeLightSources.descriptorCount = scene->lightSources.size();
	writeLightSources.pBufferInfo = lightsBufferInfos.data();

	VkDescriptorImageInfo envProbeInfo{};
	envProbeInfo.sampler = envProbeTextureImage->sampler;
	envProbeInfo.imageView = envProbeTextureImage->view;
	envProbeInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

	VkWriteDescriptorSet writeEnvProbe{};
	writeEnvProbe.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeEnvProbe.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	writeEnvProbe.dstSet = rtDescriptorSet;
	writeEnvProbe.dstBinding = 8;
	writeEnvProbe.descriptorCount = 1;
	writeEnvProbe.pImageInfo = &envProbeInfo;

	std::vector<VkWriteDescriptorSet> descriptorWrites{
		writeAC,
		writeImage,
		writeUniforms,
		writeVertexAttrib,
		writeIndices,
		writeMaterials,
		writeTextures,
		writeEnvProbe,
		writeRayCntImage,
		writeSeedImage
	};
	if (!scene->lightSources.empty()) {
		descriptorWrites.push_back(writeLightSources);
	}

	vkUpdateDescriptorSets(device, descriptorWrites.size(), descriptorWrites.data(), 0, nullptr);
}


void RayTracerNV::createSwapchainDeps() {
	createRTImage();

	VkDescriptorImageInfo descriptorImageInfo{};
	descriptorImageInfo.imageView = rtImage.view;
	descriptorImageInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
	VkWriteDescriptorSet rtImageWrite = getImageDescriptorWrite(descriptorImageInfo, 1);
	vkUpdateDescriptorSets(device, 1, &rtImageWrite, 0, nullptr);

	createRayCountBuffers();

	createRayCountImage();

	VkDescriptorImageInfo descriptorRayCountImage{};
	descriptorRayCountImage.imageView = rayCountImage.view;
	descriptorRayCountImage.imageLayout = VK_IMAGE_LAYOUT_GENERAL;
	VkWriteDescriptorSet rayCountImageWrite = getImageDescriptorWrite(descriptorRayCountImage, 9);
	vkUpdateDescriptorSets(device, 1, &rayCountImageWrite, 0, nullptr);
};


void RayTracerNV::cleanupSwapchainDeps() {
	rtImage.destroy(device);
	rayCountImage.destroy(device);
	for (auto& buffer : rayCountBuffers) {
		buffer.destroy(device);
	}
};

VkWriteDescriptorSet RayTracerNV::getImageDescriptorWrite(VkDescriptorImageInfo &descriptorImageInfo, uint32_t binding) {

	VkWriteDescriptorSet writeImage{};
	writeImage.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	writeImage.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
	writeImage.dstSet = rtDescriptorSet;
	writeImage.dstBinding = binding;
	writeImage.descriptorCount = 1;
	writeImage.pImageInfo = &descriptorImageInfo;

	return writeImage;
}

void RayTracerNV::createRayCountBuffers() {
	rayCountBuffers.clear();
	for (size_t i = 0; i < swapchainImages.size(); ++i) {
		Buffer rcBuffer;
		createBuffer(winWidth * winHeight * sizeof(uint32_t) * 2, VK_BUFFER_USAGE_TRANSFER_DST_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_CACHED_BIT, rcBuffer.buffer, rcBuffer.memory);
		rayCountBuffers.push_back(rcBuffer);
	}
}

void RayTracerNV::recordPipelineDrawCommands(VkCommandBuffer commandBuffer, int swapchainIndex) {

	traceRaysCommon(commandBuffer, swapchainIndex);

	VkImage targetImage = swapchainImages[swapchainIndex];

	transferImageLayout(commandBuffer, targetImage, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

	transferImageLayout(commandBuffer, rtImage.image, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

	VkImageCopy imageCopy{};
	imageCopy.extent = { winWidth, winHeight, 1 };
	imageCopy.srcOffset = { 0, 0, 0 };
	imageCopy.dstOffset = { 0, 0, 0 };
	imageCopy.srcSubresource = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1 };
	imageCopy.dstSubresource = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1 };

	vkCmdCopyImage(commandBuffer,
		rtImage.image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
		targetImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
		1, &imageCopy);

	transferImageLayout(commandBuffer, targetImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);

	transferImageLayout(commandBuffer, rtImage.image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL);
}

void RayTracerNV::traceRaysCommon(VkCommandBuffer commandBuffer, int swapchainIndex) {

	vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_RAY_TRACING_NV, rtPipeline);
	vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_RAY_TRACING_NV, rtLayout, 0, 1,
		&rtDescriptorSet, 0, 0);

	vkCmdResetQueryPool(commandBuffer, rtTimeQueryPool, swapchainIndex * 2, 2);

	vkCmdWriteTimestamp(commandBuffer, VK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_NV, rtTimeQueryPool, swapchainIndex * 2);

	traceRays(commandBuffer);

	vkCmdWriteTimestamp(commandBuffer, VK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_NV, rtTimeQueryPool, swapchainIndex * 2 + 1);

	transferImageLayout(commandBuffer, rayCountImage.image, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

	VkBufferImageCopy rayCountBufferCopy{};
	rayCountBufferCopy.bufferImageHeight = 0;
	rayCountBufferCopy.bufferRowLength = 0;
	rayCountBufferCopy.imageExtent = { winWidth, winHeight, 1 };
	VkImageSubresourceLayers subrec{};
	subrec.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	subrec.layerCount = 1;
	rayCountBufferCopy.imageSubresource = subrec;

	vkCmdCopyImageToBuffer(commandBuffer, rayCountImage.image, VK_IMAGE_LAYOUT_GENERAL,
		rayCountBuffers[swapchainIndex].buffer, 1, &rayCountBufferCopy);

	transferImageLayout(commandBuffer, rayCountImage.image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL);
}

void RayTracerNV::traceRays(VkCommandBuffer commandBuffer) {
	VkDeviceSize genOffset = 0;
	VkDeviceSize missOffset = rayTracingProperties.shaderGroupHandleSize;
	VkDeviceSize hitOffset = rayTracingProperties.shaderGroupHandleSize * 2;
	VkDeviceSize bindingStride = rayTracingProperties.shaderGroupHandleSize;
	auto vkCmdTraceRaysNV = PFN_vkCmdTraceRaysNV(vkGetDeviceProcAddr(device, "vkCmdTraceRaysNV"));
	vkCmdTraceRaysNV(commandBuffer,
		sbtBuffer.buffer, genOffset,
		sbtBuffer.buffer, missOffset, bindingStride,
		sbtBuffer.buffer, hitOffset, bindingStride,
		VK_NULL_HANDLE, 0, 0,
		winWidth, winHeight, 1);
}

void RayTracerNV::fillUniformBuffer() {

	UniformParams uniforms;
	Camera& cam = *scene->camera;
	glm::mat4 camBase = cam.getBase();
	uniforms.camPos = camBase[3];
	uniforms.camDir = camBase[2];
	uniforms.camUp = camBase[1];
	uniforms.camSide = camBase[0];
	uniforms.camNearFarFovYRatio = glm::vec4(cam.near, cam.far, cam.fovY, cam.fovXYRatio);

	uniforms.rtParams = rtParams;
	uniforms.numLightSources = scene->lightSources.size();

	void* data;
	vkMapMemory(device, uniformBuffer.memory, 0, sizeof(uniforms), 0, &data);
	memcpy(data, &uniforms, sizeof(uniforms));
	vkUnmapMemory(device, uniformBuffer.memory);
}

void RayTracerNV::printInitInfo() {
	std::cout << "-------<Scene info>-------" << std::endl;
	std::cout << "Num. triangles: " << scene->numTriangles << std::endl;
	std::cout << "Num. unique triangles: " << scene->numUniqueTriangles << std::endl;
	std::cout << "BLAS sizes: " << std::endl;
	float sizeTotal = 0;
	for (const auto& blas : blases) {
		float sizeKb = blas.memReq.memoryRequirements.size / 1024.f;
		std::cout << "\t" << sizeKb << "Kb" << std::endl;
		sizeTotal += sizeKb;
	}
	std::cout << "Total: " << sizeTotal << "Kb" << std::endl;
	std::cout << "TLAS size: " << tlasSize / 1024.f << "Kb" << std::endl;
}


