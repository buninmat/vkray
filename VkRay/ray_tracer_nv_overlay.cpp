﻿#include "ray_tracer_nv_overlay.h"

void RayTracerNVOverlay::createImageViews() {
	swapchainImageViews.resize(swapchainImages.size());
	for (int i = 0; i < swapchainImages.size(); ++i) {
		VkImageViewCreateInfo createInfo{};
		createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		createInfo.image = swapchainImages[i];
		createInfo.format = swapchainImageFormat;
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		// channels mapping
		createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		// mipmap, layers (eg for VR)
		createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		createInfo.subresourceRange.baseMipLevel = 0;
		createInfo.subresourceRange.levelCount = 1;
		createInfo.subresourceRange.baseArrayLayer = 0;
		createInfo.subresourceRange.layerCount = 1;
		log.check_failure(vkCreateImageView(device, &createInfo, nullptr, &swapchainImageViews[i]),
			"Failed to create an image view",
			"...swapchain image view");
	}
}

void RayTracerNVOverlay::createRenderPass() {
	VkAttachmentDescription colorAttachment{};
	colorAttachment.format = swapchainImageFormat; //expected framebuffer format
	colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
	colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR; //clear before drawing
	colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE; //store what u draw
	colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	VkAttachmentReference colorAttachmentRef{};
	colorAttachmentRef.attachment = 0; // 'out color' location
	colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkSubpassDescription subpass{};
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &colorAttachmentRef;

	VkSubpassDependency dependency{};
	//memory dependency for image layout transition
	dependency.srcSubpass = VK_SUBPASS_EXTERNAL; //implicit previous subpass
	dependency.dstSubpass = 0; //current subpass
	dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.srcAccessMask = 0; //wait for previous subpass to finish reading (_READ_BIT is redunant in src because no cache flushes happen)
	dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT; //after that we can write
	// (Could have been avaoided by witing on a swapchain image semaphore at TOP_OF_PIPE stage, where the transition occurs by default.
	// In this case though image is not yet avaliable at that stage)

	VkRenderPassCreateInfo renderPassInfo{};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.attachmentCount = 1;
	renderPassInfo.pAttachments = &colorAttachment;
	renderPassInfo.subpassCount = 1;
	renderPassInfo.pSubpasses = &subpass;
	renderPassInfo.dependencyCount = 1;
	renderPassInfo.pDependencies = &dependency;

	log.check_failure(vkCreateRenderPass(device, &renderPassInfo, nullptr, &renderPass),
		"Failed to create render pass",
		"...render pass created");
}

struct VertexProperties {

	static VkVertexInputBindingDescription getBingingDescription() {
		VkVertexInputBindingDescription bindingDescription{};
		bindingDescription.binding = 0; // index in bindings array
		bindingDescription.stride = sizeof(Vertex);
		bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
		return bindingDescription;
	}

	static const VkFormat vec3Format = VK_FORMAT_R32G32B32_SFLOAT;
	static const VkFormat vec2Format = VK_FORMAT_R32G32_SFLOAT;

	static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions() {
		std::vector<VkVertexInputAttributeDescription> descriptions{
			{0, 0, vec3Format, offsetof(Vertex, pos)},
			//{1, 0, vec3Format, offsetof(Vertex, normal)},
			//{2, 0, vec2Format, offsetof(Vertex, uv)}
		};
		return descriptions;
	}
};

void RayTracerNVOverlay::createGraphicsPipeline() {
	auto vertexShaderBinary = readBinary("shaders/overlay-vert.spv");
	auto fragmentShaderBinary = readBinary("shaders/overlay-frag.spv");
	VkShaderModule vertexShaderMoodule = createShaderModule(vertexShaderBinary);
	VkShaderModule fragmentShaderMoodule = createShaderModule(fragmentShaderBinary);

	VkPipelineShaderStageCreateInfo vertexStageInfo{};
	vertexStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	vertexStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
	vertexStageInfo.module = vertexShaderMoodule;
	vertexStageInfo.pName = "main";
	vertexStageInfo.pSpecializationInfo = nullptr; //compile-time constants

	VkPipelineShaderStageCreateInfo fragmentStageInfo{};
	fragmentStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	fragmentStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	fragmentStageInfo.module = fragmentShaderMoodule;
	fragmentStageInfo.pName = "main";

	VkPipelineShaderStageCreateInfo shaderStageInfos[] = { vertexStageInfo, fragmentStageInfo };

	VkPipelineVertexInputStateCreateInfo inputStateInfo{};
	inputStateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	auto vertexBindingDescriptions = VertexProperties::getBingingDescription();
	auto attributeDiscriptions = VertexProperties::getAttributeDescriptions();
	inputStateInfo.vertexBindingDescriptionCount = 1;
	inputStateInfo.pVertexBindingDescriptions = &vertexBindingDescriptions;
	inputStateInfo.vertexAttributeDescriptionCount = attributeDiscriptions.size();
	inputStateInfo.pVertexAttributeDescriptions = attributeDiscriptions.data();

	VkPipelineInputAssemblyStateCreateInfo assemblyStateInfo{};
	assemblyStateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	assemblyStateInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	//TODO break strips with index 0xFFFF
	assemblyStateInfo.primitiveRestartEnable = VK_FALSE;

	VkViewport viewport{};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.height = (float)swapchainExtent.height;
	viewport.width = (float)swapchainExtent.width;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;
	VkRect2D scissor{};
	scissor.extent = swapchainExtent;
	scissor.offset = viewportOffset;
	VkPipelineViewportStateCreateInfo viewportStateInfo{};
	viewportStateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportStateInfo.viewportCount = 1;
	viewportStateInfo.pViewports = &viewport;
	viewportStateInfo.scissorCount = 1;
	viewportStateInfo.pScissors = &scissor;

	VkPipelineRasterizationStateCreateInfo rasterizerInfo{};
	rasterizerInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	//TODO for clamping to near and far plane enable a device feature and this option (for shadows)
	rasterizerInfo.depthClampEnable = VK_FALSE;
	//TODO cut the pipeline on rasterizer input
	rasterizerInfo.rasterizerDiscardEnable = VK_FALSE;
	//TODO set LINE for wireframe etc. GPU feature required
	rasterizerInfo.polygonMode = VK_POLYGON_MODE_FILL;
	rasterizerInfo.lineWidth = 1.0f; //change only with wideLines GPU feature
	rasterizerInfo.cullMode = VK_CULL_MODE_NONE;//VK_CULL_MODE_BACK_BIT;
	rasterizerInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;

	//TODO enable and configure other fields (for shadows)
	rasterizerInfo.depthBiasEnable = VK_FALSE;

	VkPipelineMultisampleStateCreateInfo multisamplingInfo{};
	multisamplingInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisamplingInfo.sampleShadingEnable = VK_FALSE;
	multisamplingInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	//TODO configure multisampling 

	//TODO depth/stencil VkPipelineDepthStencilStateCreateInfo

	VkPipelineColorBlendAttachmentState colorBlendAttachment{};
	colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	colorBlendAttachment.blendEnable = VK_TRUE;
	// Alpha blending
	colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
	colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
	colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
	colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

	VkPipelineColorBlendStateCreateInfo colorBlending{};
	colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlending.logicOpEnable = VK_FALSE;
	colorBlending.attachmentCount = 1;
	colorBlending.pAttachments = &colorBlendAttachment;

	VkPipelineDynamicStateCreateInfo dynamicState{};
	dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	VkDynamicState dynamicStates[] = {
		VK_DYNAMIC_STATE_VIEWPORT,
		VK_DYNAMIC_STATE_LINE_WIDTH,
	};
	dynamicState.dynamicStateCount = 2;
	dynamicState.pDynamicStates = dynamicStates;

	VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = 1;
	pipelineLayoutInfo.pSetLayouts = &discriptorSetLayout;

	log.check_failure(vkCreatePipelineLayout(device, &pipelineLayoutInfo, nullptr, &pipelineLayout),
		"Failed to create pipeline layout!");

	VkGraphicsPipelineCreateInfo pipelineInfo{};
	pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;

	pipelineInfo.stageCount = 2;
	pipelineInfo.pStages = shaderStageInfos;

	pipelineInfo.pVertexInputState = &inputStateInfo;
	pipelineInfo.pInputAssemblyState = &assemblyStateInfo;
	pipelineInfo.pViewportState = &viewportStateInfo;
	pipelineInfo.pRasterizationState = &rasterizerInfo;
	pipelineInfo.pMultisampleState = &multisamplingInfo;
	pipelineInfo.pDepthStencilState = nullptr;
	pipelineInfo.pColorBlendState = &colorBlending;
	pipelineInfo.pDynamicState = nullptr;

	pipelineInfo.layout = pipelineLayout;

	pipelineInfo.renderPass = renderPass;
	pipelineInfo.subpass = 0;

	// VK_PIPELINE_CREATE_DERIVATIVE_BIT flag
	pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;
	pipelineInfo.basePipelineIndex = -1;

	log.check_failure(vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline),
		"Failed to create graphics pipeline!",
		"...graphics pipeline created");

	vkDestroyShaderModule(device, vertexShaderMoodule, nullptr);
	vkDestroyShaderModule(device, fragmentShaderMoodule, nullptr);
}

void RayTracerNVOverlay::createDescriptorSetLayout() {
	VkDescriptorSetLayoutBinding layoutBinding{};
	layoutBinding.binding = 0;
	layoutBinding.descriptorCount = 1;
	layoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	layoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT; // change to reference the buffer from ther stages
	layoutBinding.pImmutableSamplers = nullptr; // set for image samplings

	VkDescriptorSetLayoutCreateInfo createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	createInfo.bindingCount = 1;
	createInfo.pBindings = &layoutBinding;

	log.check_failure(vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &discriptorSetLayout),
		"Failed to create descriptor set layout!");
}

void RayTracerNVOverlay::createFramebuffers() {
	swapchainFramebuffers.resize(swapchainImageViews.size());

	for (int i = 0; i < swapchainImageViews.size(); i++) {
		VkFramebufferCreateInfo framebufferInfo{};
		framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferInfo.renderPass = renderPass;
		framebufferInfo.attachmentCount = 1;
		framebufferInfo.pAttachments = &swapchainImageViews[i];
		framebufferInfo.width = swapchainExtent.width;
		framebufferInfo.height = swapchainExtent.height;
		framebufferInfo.layers = 1;

		log.check_failure(vkCreateFramebuffer(device, &framebufferInfo, nullptr, &swapchainFramebuffers[i]),
			"Failed to create framebuffer!",
			"...framebuffer created");
	}

}

void RayTracerNVOverlay::createRTImageSampler() {

	VkSamplerCreateInfo samplerInfo{};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.minFilter = VK_FILTER_NEAREST;
	samplerInfo.magFilter = VK_FILTER_NEAREST;
	samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
	samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
	samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
	samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_WHITE;
	samplerInfo.anisotropyEnable = VK_FALSE;
	samplerInfo.maxAnisotropy = 1.0f;
	samplerInfo.compareEnable = VK_FALSE;
	samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
	samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	samplerInfo.mipLodBias = 0.0f;
	samplerInfo.minLod = 0.0f;
	samplerInfo.maxLod = 0.0f;
	log.check_failure(vkCreateSampler(device, &samplerInfo, nullptr, &rtImageSampler),
		"Failed to create RT image sampler");
}

void RayTracerNVOverlay::createDescriptorPool() {
	VkDescriptorPoolSize poolSize{};
	poolSize.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	poolSize.descriptorCount = swapchainImages.size() * 2;

	VkDescriptorPoolCreateInfo createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	createInfo.poolSizeCount = 1;
	createInfo.pPoolSizes = &poolSize;
	createInfo.maxSets = swapchainImages.size() * 2;

	log.check_failure(vkCreateDescriptorPool(device, &createInfo, nullptr, &descriptorPool),
		"Failed to create descriptor pool!");
}

void RayTracerNVOverlay::createDescriptorSets() {
	VkDescriptorSetAllocateInfo allocInfo{};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = descriptorPool;
	allocInfo.descriptorSetCount = swapchainImages.size() * 2;
	std::vector<VkDescriptorSetLayout> layouts(swapchainImages.size() * 2, discriptorSetLayout);
	allocInfo.pSetLayouts = layouts.data();

	descriptorSets.resize(swapchainImages.size() * 2);

	log.check_failure(vkAllocateDescriptorSets(device, &allocInfo, descriptorSets.data()),
		"Failed to allocate descriptor sets!");

	for (int i = 0; i < swapchainImages.size(); i++) {
		VkDescriptorImageInfo textImageInfo{};
		textImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		textImageInfo.imageView = textObject->textureImage->view;
		textImageInfo.sampler = textObject->textureImage->sampler;

		VkWriteDescriptorSet textImageWrite{};
		textImageWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		textImageWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		textImageWrite.descriptorCount = 1;
		textImageWrite.dstBinding = 0;
		textImageWrite.dstSet = descriptorSets[i];
		textImageWrite.pImageInfo = &textImageInfo;

		VkDescriptorImageInfo rtImageInfo{};
		rtImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		rtImageInfo.imageView = rtImage.view;
		rtImageInfo.sampler = rtImageSampler;

		VkWriteDescriptorSet rtImageWrite{};
		rtImageWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		rtImageWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		rtImageWrite.descriptorCount = 1;
		rtImageWrite.dstBinding = 0;
		rtImageWrite.dstSet = descriptorSets[i + swapchainImages.size()];
		rtImageWrite.pImageInfo = &rtImageInfo;

		std::vector<VkWriteDescriptorSet> dsWrites = { rtImageWrite, textImageWrite };

		vkUpdateDescriptorSets(device, dsWrites.size(), dsWrites.data(), 0, nullptr);
	}
}

#include "fonts/font-win.h"

void renderChar(uint32_t* frame, int frameWidth, char c, int yrow, int xcolumn, int size,
	uint32_t forecolor, uint32_t backcolor) {
	font_descriptor_t font = FONT;
	int char_ind = c - font.firstchar;
	const uint16_t* bits = font.bits + char_ind * font.height;
	int width = font.width != 0 ? font.width[char_ind] : font.maxwidth;
	for (int i = 0; i < font.height; i++) {
		uint16_t row = bits[i];

		for (int j = 0; j < width; j++) {
			uint32_t color = ((row & 0x8000) != 0) ? forecolor : backcolor;
			for (int k = 0; k < size; ++k) {
				for (int m = 0; m < size; ++m) {
					int y = i * size + yrow + k;
					int x = j * size + xcolumn + m;
					frame[y * frameWidth + x] = color;
				}
			}

			row <<= 1;
		}
	}
}

int renderStr(uint32_t *frame, int frameWidth, const char* str, int yrow, int xcolumn, int size, int linespace,
	uint32_t forecolor, uint32_t backcolor) {

	font_descriptor_t font = FONT;
	int yoffset = 0;
	int xoffset = 0;
	for (int i = 0; i < strlen(str); i++) {
		if (str[i] == '\n') {
			yoffset += font.height * size + linespace;
			xoffset = 0;
			continue;
		}
		int char_ind = str[i] - font.firstchar;
		int is_valid = char_ind < font.size;
		if (is_valid) {
			renderChar(frame, frameWidth, str[i], yrow + yoffset, xcolumn + xoffset, size, forecolor, backcolor);
		}
		int width = font.width != 0 ? font.width[char_ind] : font.maxwidth;
		xoffset += width * size;
	}
	return yoffset;
}

struct VertData {
	glm::vec2 pos;
	glm::vec2 uv;
};
std::vector<VertData> rectVertices = {
	{{-1.0f, 1.0f}, {0.0f, 1.0f}},
	{{1.0f, 1.0f}, {1.0f, 1.0f}},
	{{1.0f, -1.0f}, {1.0f, 0.0f}},
	{{-1.0f, -1.0f}, {0.0f, 0.0f}}
};
std::vector<uint32_t> rectIndices = { 0, 3, 1, 1, 3, 2 };

void RayTracerNVOverlay::createBackgObject() {
	GeometryObject backgRect;
	backgRect.indices = rectIndices;

	for (VertData vert : rectVertices) {
		glm::vec4 norm = glm::vec4(0.0f, 0.0f, 1.0f, 0.0f);
		glm::vec4 uv = glm::vec4(vert.uv, 0.0f, 0.0f);
		
		backgRect.vertices.push_back(
			{ glm::vec4(vert.pos.x, vert.pos.y, 0.2f, 0.0f), norm, uv });
	}


	backgObject = std::make_unique<OverlayObject>(backgRect);
}

void RayTracerNVOverlay::createTextObject(glm::vec2 extent, glm::vec2 offset) {
	GeometryObject textRect;
	textRect.indices = rectIndices;

	for (VertData vert : rectVertices) {
		glm::vec4 norm = glm::vec4(0.0f, 0.0f, 1.0f, 0.0f);
		glm::vec4 uv = glm::vec4(vert.uv, 0.0f, 0.0f);
		textRect.vertices.push_back(
			{ glm::vec4(vert.pos.x * extent.x + offset.x, vert.pos.y * extent.y + offset.y, 0.0f, 0.0f), norm, uv });
	}

	std::vector<uint32_t> frame(textFrameH * textFrameW);

	Texture texture;
	texture.data = reinterpret_cast<unsigned char*>(frame.data());
	texture.width = textFrameW;
	texture.height = textFrameH;

	textRect.material.diffuseTexture = std::make_unique<Texture>(texture);
	textObject = std::make_unique<OverlayObject>(textRect);
}

void RayTracerNVOverlay::recordPipelineDrawCommands(VkCommandBuffer commandBuffer, int swapchainIndex) {
	traceRaysCommon(commandBuffer, swapchainIndex);

	transferImageLayout(commandBuffer, rtImage.image, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

	VkRenderPassBeginInfo renderPassBegin{};
	renderPassBegin.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassBegin.renderPass = renderPass;
	renderPassBegin.framebuffer = swapchainFramebuffers[swapchainIndex];

	renderPassBegin.renderArea.offset = viewportOffset;
	renderPassBegin.renderArea.extent = swapchainExtent;

	VkClearValue clearColor = { 0.f, 0.f, 0.f, 0.0f };
	renderPassBegin.clearValueCount = 1;
	renderPassBegin.pClearValues = &clearColor;

	vkCmdBeginRenderPass(commandBuffer, &renderPassBegin, VK_SUBPASS_CONTENTS_INLINE);

	vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);

	VkDeviceSize offsets[] = { 0 };

	vkCmdBindVertexBuffers(commandBuffer, 0, 1, &backgObject->vertexBuffer.buffer, offsets);
	vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1,
		&descriptorSets[swapchainIndex + swapchainImages.size()], 0, nullptr);
	vkCmdBindIndexBuffer(commandBuffer, backgObject->indexBuffer.buffer, 0, VK_INDEX_TYPE_UINT32);
	vkCmdDrawIndexed(commandBuffer, backgObject->indexBuffer.count, 1, 0, 0, 0);
	
	vkCmdBindVertexBuffers(commandBuffer, 0, 1, &textObject->vertexBuffer.buffer, offsets);
	vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1,
		&descriptorSets[swapchainIndex], 0, nullptr);
	vkCmdBindIndexBuffer(commandBuffer, textObject->indexBuffer.buffer, 0, VK_INDEX_TYPE_UINT32);
	vkCmdDrawIndexed(commandBuffer, textObject->indexBuffer.count, 1, 0, 0, 0);
	
	vkCmdEndRenderPass(commandBuffer);

	transferImageLayout(commandBuffer, rtImage.image, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL);
}

void RayTracerNVOverlay::printProfilingInfo() {
	static RTFramesSpan rtRecentStats = rtTotalStats;
	static RTFramesSpan rtLastStats = rtTotalStats;

	rtRecentStats = rtTotalStats - rtLastStats;
	rtLastStats = rtTotalStats;

	std::stringstream text;
	if (!hideInfo) {
		ulongToStr(text, scene->numTriangles);
		text << " triangles" << std::endl;
		text << "(";
		ulongToStr(text, scene->numUniqueTriangles);
		text << " unique triangles)" << std::endl;
		text << winWidth << " X " << winHeight << std::endl;
		if (recentFramesTime.gpuTimeMs > 0 || rtParams.sampleInd == rtParams.numSamples) {
			text << getFPS(recentFramesTime) << " FPS" << std::endl;
			ulongToStr(text, getRPS(recentFramesTime, rtRecentStats));
			text << " rays/s" << std::endl;
			ulongToStr(text, curRaysPerSample);
			text << " sec.rays/samp" << std::endl;
			ulongToStr(text, curShadowRaysPerSample);
			text << " shad.rays/samp" << std::endl;
		}
		else {
			text << "..." << std::endl;
		}
		text << "(up/down)* Recursion depth: ";
		ulongToStr(text, rtParams.recursionDepth);
		text << std::endl;
		text << "([0-9]+ + enter)* Num. samples: ";
		ulongToStr(text, rtParams.sampleInd);
		text << "/";
		ulongToStr(text, rtParams.numSamples);
		text << std::endl;
		text << "(space to hide/show)" << std::endl;
	}

	std::vector<uint32_t> frame(textFrameH * textFrameW);
	
	Texture texture;
	texture.data = reinterpret_cast<unsigned char*>(frame.data());
	texture.width = textFrameW;
	texture.height = textFrameH;

	renderStr(frame.data(), textFrameW, text.str().data(), 0, 0, 1, 5, 0xfc'00'cc'cc, 0x0);

	textObject->textureImage->updateImage(texture);
}
