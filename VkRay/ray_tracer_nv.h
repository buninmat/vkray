#pragma once
#include "vkray.h"
#include "vulkan_base.h"

struct RayTracerNV : public VulkanBase
{

	struct TextureImage {
		VkImage image;
		VkDeviceMemory memory;
		VkImageView view;
		VkSampler sampler;

		VkDevice device;

		TextureImage(VkDevice device, Texture& texture, VkFilter filter = VK_FILTER_LINEAR): device(device) {

			runningInstance->createImage(texture.width, texture.height, VK_FORMAT_R8G8B8A8_SRGB,
				VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				image, memory);

			updateImage(texture);

			view = runningInstance->createImageView(image, VK_FORMAT_R8G8B8A8_SRGB);

			VkSamplerCreateInfo samplerInfo{};
			samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
			samplerInfo.minFilter = filter;
			samplerInfo.magFilter = filter;
			samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
			samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
			samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
			samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_WHITE;
			//TODO: anisotropy (enable device feature)
			samplerInfo.anisotropyEnable = VK_FALSE;
			samplerInfo.maxAnisotropy = 1.0f;
			//for percentage-closer filtering
			samplerInfo.compareEnable = VK_FALSE;
			samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
			//mipmap
			samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
			samplerInfo.mipLodBias = 0.0f;
			samplerInfo.minLod = 0.0f;
			samplerInfo.maxLod = 0.0f;

			if (vkCreateSampler(device, &samplerInfo, nullptr, &sampler) != VK_SUCCESS) {
				throw std::runtime_error("Failed to create sampler");
			}
		}

		void updateImage(Texture& texture) {
			Buffer stagingBuffer;
			VkDeviceSize size = texture.width * texture.height * 4;
			runningInstance->createBuffer(size,
				VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
				stagingBuffer.buffer, stagingBuffer.memory);

			void* data;
			vkMapMemory(device, stagingBuffer.memory, 0, size, 0, &data);
			memcpy(data, texture.data, size);
			vkUnmapMemory(device, stagingBuffer.memory);

			VkCommandBuffer commandBuffer = runningInstance->beginNewCommandBuffer();

			runningInstance->transferImageLayout(commandBuffer, image, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

			VkBufferImageCopy region{};
			region.bufferImageHeight = 0; // spaces between pixels
			region.bufferRowLength = 0;
			region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			region.imageSubresource.layerCount = 1;
			region.imageSubresource.mipLevel = 0;
			region.imageSubresource.baseArrayLayer = 0;
			region.imageOffset = { 0, 0, 0 };
			region.imageExtent = { static_cast<uint32_t>(texture.width), static_cast<uint32_t>(texture.height), 1 };

			vkCmdCopyBufferToImage(commandBuffer, stagingBuffer.buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

			runningInstance->transferImageLayout(commandBuffer, image,
				VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

			runningInstance->endAndFlushCommandBuffer(commandBuffer);
			stagingBuffer.destroy(device);
		}

		~TextureImage(){
			vkDestroyImage(device, image, nullptr);
			vkFreeMemory(device, memory, nullptr);
			vkDestroyImageView(device, view, nullptr);
			vkDestroySampler(device, sampler, nullptr);
		}
	};

	struct RTMaterial {
		glm::vec4 diffuse;
		uint32_t diffuseTexId;
		float specular;
		float sharpness;
	};

	struct RTLightSource {
		glm::vec4 pos;
		glm::vec4 color;
		uint32_t isDirectional;
	};

	struct RTSceneObject : public SceneObject {

		Buffer indexBuffer;
		Buffer vertexBuffer;

		RTMaterial material;

		std::unique_ptr<TextureImage> textureImage;

		RTSceneObject(const GeometryObject& geometry) {
			createElementsBuffer(vertexBuffer, geometry.vertices,
				VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT);
			createElementsBuffer(indexBuffer, geometry.indices,
				VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT);
			if (geometry.material.diffuseTexture.get() != nullptr) {
				textureImage = std::make_unique<TextureImage>(device, *geometry.material.diffuseTexture);
			}
			material.diffuse = geometry.material.diffuse;
			material.diffuseTexId = -1.0f;
			material.sharpness = geometry.material.sharpness;
			material.specular = geometry.material.specular;
			
			numGeometryTriangles = geometry.numTriangles();
		}

		~RTSceneObject() {
			vertexBuffer.destroy(device);
			indexBuffer.destroy(device);
			textureImage.reset();
		}
	};

	class RTScene : public Scene {

		SceneObject* createSceneObject(const GeometryObject& geomObject) override {
			return new RTSceneObject(geomObject);
		}

	};

	#define SAMPLES_PER_FRAME 5

	struct RTParameters {
		uint32_t recursionDepth = 2;
		uint32_t numSamples = SAMPLES_PER_FRAME;
		uint32_t sampleInd = 0;
		uint32_t gammaCorrect = false;
		uint32_t invGammaCorrect = false;
		uint32_t readBGR = false;
		uint32_t writeBGR = false;
	} rtParams;

	struct UniformParams {
		glm::vec4 camPos;
		glm::vec4 camDir;
		glm::vec4 camUp;
		glm::vec4 camSide;
		glm::vec4 camNearFarFovYRatio;
		uint32_t numLightSources;
		RTParameters rtParams;
	};

	struct GeometryInstance { // must be defined this way
		glm::mat3x4 transform;
		uint32_t instanceId : 24;
		uint32_t mask : 8;
		uint32_t instanceOffset : 24;
		uint32_t flags : 8;
		uint64_t accelerationStructureHandle;
	};

	struct BLAS {
		VkAccelerationStructureNV blas;
		VkMemoryRequirements2KHR memReq;
		std::shared_ptr<VkGeometryNV> geometry;
		VkAccelerationStructureInfoNV info;
		VkDeviceMemory memory;
		uint64_t handle;
		RTSceneObject* sceneObject;
	};

	std::vector<BLAS> blases;

	VkAccelerationStructureNV tlas;
	VkDeviceMemory tlasMem;

	Buffer uniformBuffer;
	Buffer sbtBuffer;
	Buffer geomInstanceBuffer;
	std::vector<Buffer> materialsBuffers;
	std::vector<Buffer> lightSourcesBuffers;

	struct Image {
		VkImage image;
		VkDeviceMemory memory;
		VkImageView view;
		void destroy(VkDevice device) {
			vkDestroyImageView(device, view, nullptr);
			vkDestroyImage(device, image, nullptr);
			vkFreeMemory(device, memory, nullptr);
		}
	};

	Image rtImage;
	Image seedImage;
	Image rayCountImage;

	VkShaderModule raygenShaderMoodule;
	VkShaderModule chitShaderMoodule;
	VkShaderModule missShaderMoodule;

	VkPipeline rtPipeline;
	VkPipelineLayout rtLayout;
	VkDescriptorSetLayout rtDescriptorSetLayout;

	VkDescriptorPool descriptorPool;
	VkDescriptorSet rtDescriptorSet;

	VkQueryPool rtTimeQueryPool;

	std::unique_ptr<TextureImage> envProbeTextureImage;

	std::vector<Buffer> rayCountBuffers;

	using raysCountVec = glm::vec<2, uint32_t>;
	
	virtual void recordPipelineDrawCommands(VkCommandBuffer commandBuffer, int swapchainIndex) override;

	void traceRaysCommon(VkCommandBuffer commandBuffer, int swapchainIndex);

	void traceRays(VkCommandBuffer commandBuffer);

	struct RTFramesSpan{
		unsigned long traceRaysTimeMs = 0;
		unsigned long secRaysCount = 0;
		unsigned long shadowRaysCount = 0;
		unsigned long numSamples = 0;
		RTFramesSpan operator -(RTFramesSpan other) {
			return {
				traceRaysTimeMs - other.traceRaysTimeMs,
				secRaysCount - other.secRaysCount,
				shadowRaysCount - other.shadowRaysCount,
				numSamples - other.numSamples
			};
		}
	};

	int curRaysPerSample;
	int curShadowRaysPerSample;

	RTFramesSpan rtTotalStats;

	int updatedImageIndex = 0;

	void frameUpdate(int swapchainIndex) override;

	void onKeyAction(int key, int action) {
		updateRecursionDepth(key, action);
		updateMaxSamples(key, action);
	}

	void updateRecursionDepth(int key, int action);

	void updateMaxSamples(int key, int action);

	inline int getFPS(FramesTimeSpan span) {
		return round(span.framesCnt * 1000.f / span.cpuTimeMs);
	}

	inline unsigned long getRPS(FramesTimeSpan span, RTFramesSpan rtStats) {
		unsigned long primRays = span.framesCnt * winWidth * winHeight;
		return span.gpuTimeMs == 0? 0: (primRays + rtStats.secRaysCount + rtStats.shadowRaysCount) * 1000 / span.gpuTimeMs;
	}

	void printFinalTimings();

	raysCountVec getSecondaryRaysCount(int swapchainIndex);

	void compactBlasses(VkQueryPool blasSizesQueryPool);

	void createSwapchainDeps() override;

	VkWriteDescriptorSet getImageDescriptorWrite(VkDescriptorImageInfo &descriptorImageInfo, uint32_t binding);

	void cleanupSwapchainDeps() override;

	void printProfilingInfo() override {};

	void initRayTracing();

	size_t tlasSize;

	void createAccelerationStructures();

	void createRayTracingPipeline();

	void createRTShaderBindingTable();

	void createRTImage();

	void createRayCountImage();

	void createSeedImage();

	void createRTDescriptorSets();

	void createUniformBuffer();

	void createMaterialsBuffers();

	void createLightSourceBuffers();

	void fillUniformBuffer();

	uint32_t getTexturesCount();

	void createRayCountBuffers();

	void printInitInfo();

	void saveImage();

public:
	RayTracerNV(Arguments arguments) : VulkanBase(arguments) {
		scene = std::make_unique<RTScene>();
		scene->init(arguments.sceneType, arguments.useLightSources);
		envProbeTextureImage = std::make_unique<TextureImage>(device, *scene->envProbe);
		rtParams.gammaCorrect = true;
		rtParams.invGammaCorrect = true;
		rtParams.readBGR = true;
		rtParams.writeBGR = true;
		initRayTracing();
		createAccelerationStructures();
		createRayTracingPipeline();
		createRTShaderBindingTable();
		createRTImage();
		createSeedImage();
		createRayCountImage();
		createUniformBuffer();
		createMaterialsBuffers();
		createLightSourceBuffers();
		createRTDescriptorSets();
		createTimestampQueryPool(rtTimeQueryPool);
		createRayCountBuffers();
		if (!arguments.silent) {
			printInitInfo();
		}
	}

	~RayTracerNV() {
		if (arguments.profile) {
			printFinalTimings();
		}

		vkDestroyQueryPool(device, rtTimeQueryPool, nullptr);
		vkDestroyDescriptorPool(device, descriptorPool, nullptr);
		vkDestroyDescriptorSetLayout(device, rtDescriptorSetLayout, nullptr);
		uniformBuffer.destroy(device);
		for (auto& buffer : materialsBuffers) {
			buffer.destroy(device);
		}
		for (auto& buffer : lightSourcesBuffers) {
			buffer.destroy(device);
		}
		for (auto& buffer : rayCountBuffers) {
			buffer.destroy(device);
		}

		rtImage.destroy(device);
		rayCountImage.destroy(device);
		seedImage.destroy(device);

		sbtBuffer.destroy(device);
		vkDestroyPipeline(device, rtPipeline, nullptr);
		vkDestroyPipelineLayout(device, rtLayout, nullptr);
		geomInstanceBuffer.destroy(device);
		vkDestroyShaderModule(device, raygenShaderMoodule, nullptr);
		vkDestroyShaderModule(device, chitShaderMoodule, nullptr);
		vkDestroyShaderModule(device, missShaderMoodule, nullptr);

		auto vkDestroyAccelerationStructureNV = PFN_vkDestroyAccelerationStructureNV(vkGetDeviceProcAddr(device, "vkDestroyAccelerationStructureNV"));
		for (auto& blas : blases) {
			vkDestroyAccelerationStructureNV(device, blas.blas, nullptr);
			vkFreeMemory(device, blas.memory, nullptr);
		}
		vkDestroyAccelerationStructureNV(device, tlas, nullptr);
		vkFreeMemory(device, tlasMem, nullptr);

	}
};

