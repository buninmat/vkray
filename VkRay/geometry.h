#pragma once
#include "vkray.h"
#include "texture.h"

struct Material {
	glm::vec4 diffuse = { 1.0f, 1.0f, 1.0f, 1.0f };
	std::unique_ptr<Texture> diffuseTexture;
	float specular = 0.5;
	float sharpness = 1.;
};

struct GeometryObject {
	std::string name;
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;

	Material material;

	GeometryObject() {}

	GeometryObject(std::string name) :GeometryObject(name, false) {}

	GeometryObject(std::string name, bool useDump) :name(name) {
		if (useDump) {
			if (!loadDump()) {
				loadObj();
				saveDump();
			}
		}
		else {
			loadObj();
		}
	}

	size_t numTriangles() const{
		return indices.size() / 3;
	}

	void saveDump();

	bool loadDump();

	void loadObj();
	
	static GeometryObject* makeTriangle() {
		auto* triangle = new GeometryObject();
		triangle->vertices = {
			{{-0.5f, -0.5f, 1.0f, 0.0f}, {1.0f, 0.0f, 0.0f, 0.0f}},
			{{0.5f, -0.5f, 1.0f, 0.0f}, {0.0f, 1.0f, 0.0f, 0.0f}},
			{{0.0f, 0.5f, 1.0f, 0.0f}, {0.0f, 0.0f, 1.0f, 0.0f}}
		};
		triangle->indices = { 0, 1, 2 };
		return triangle;
	}
	
	static GeometryObject* makeCube();

	static GeometryObject* makeUVSphere(float radius, int steps);

	static GeometryObject* makeTorus(float radius, float ringRadius, int uSteps, int vSteps);

	static GeometryObject* makeRectangle(float side) {
		auto* plane = new GeometryObject();
		struct VertData {
			glm::vec2 pos;
			glm::vec2 uv;
		};
		std::vector<VertData> verts = {
			{{-1.0f, 1.0f}, {0.0f, 1.0f}},
			{{1.0f, 1.0f}, {1.0f, 1.0f}},
			{{1.0f, -1.0f}, {1.0f, 0.0f}},
			{{-1.0f, -1.0f}, {0.0f, 0.0f}}
		};
		plane->indices = { 0, 3, 1, 1, 3, 2 };
		for (VertData vert : verts) {
			plane->vertices.push_back(
				{ glm::vec4(vert.pos.x, 0.0f, vert.pos.y, 0.0f) * side,
				glm::vec4(0.0f, 1.0f, 0.0f, 0.0f),
				glm::vec4(vert.uv, 0.0f, 0.0f) });
		}
		plane->material.diffuseTexture = std::make_unique<Texture>("textures/logo.png");
		return plane;
	}
};