#include "texture.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

Texture::Texture(std::string file, bool hdr) {
	if (hdr) {
	}
	else {
		stbi = true;
		data = stbi_load(file.data(), &width, &height, &nChannels, STBI_rgb_alpha);
	}
}

Texture::~Texture() {
	if (stbi) {
		stbi_image_free(data);
	}
}
