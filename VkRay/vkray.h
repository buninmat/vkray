#pragma once

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp>

#include <iostream>
#include <vector>
#include <string>
#include <optional>
#include <set>
#include <algorithm>
#include <fstream>
#include <array>
#include <chrono>
#include <unordered_map>
#include <filesystem>
#include <sstream>
#include <numeric>
#include "utils.h"

//aligned according to std140
struct Vertex {
	glm::vec4 pos;
	glm::vec4 normal;
	glm::vec4 uv;

	bool operator == (const Vertex& other) const;
};

enum SceneType {
	MONKEYS,
	BUNNY,
	BUDDHAS,
	INSTANCED_BUDDHAS
};

struct Arguments {
	SceneType sceneType;
	bool useLightSources;
	bool silent;
	bool profile;
	bool countRays;
	bool offscreen;
	int numSamples;
	int maxBounces;
	int minBounces;
	int maxFrames = -1; // if not -1 terminate after n frames
	int profilingPeriodSec;
	std::string imagePath;
};
