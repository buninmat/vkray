#pragma once
#include "vkray.h"

struct Log {
	bool silent = false;

	void check_failure(bool succ, const char* failureMessage, const char* successMessage = nullptr) {
		if (check_warning(succ, failureMessage, successMessage)) {
			exit(EXIT_FAILURE);
		}
	}

	void check_failure(VkResult result, const char* failureMessage, const char* successMessage = nullptr) {
		check_failure(result == VK_SUCCESS, failureMessage, successMessage);
	}

	bool check_warning(VkResult result, const char* failureMessage, const char* successMessage = nullptr) {
		return check_warning(result == VK_SUCCESS, failureMessage, successMessage);
	}

	bool check_warning(bool succ, const char* failureMessage, const char* successMessage = nullptr) {
		if (succ) {
			if (successMessage != nullptr) {
				message(successMessage);
			}
		}else {
			warning(failureMessage);
		}
		return !succ;
	}

	void failure(const char* msg) {
		warning(msg);
		exit(EXIT_FAILURE);
	}

	void message(const char* msg) {
		if (!silent) {
			std::cout << msg << std::endl;
		}
	}

	void warning(const char* msg) {
		message(msg);
		std::cerr << msg << std::endl;
	}
};
