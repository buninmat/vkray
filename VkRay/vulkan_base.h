#pragma once
#include "vkray.h"
#include "geometry.h"
#include "log.h"

struct SwapchainProperties {
	VkSurfaceCapabilitiesKHR surfaceCapabilities;
	std::vector<VkSurfaceFormatKHR> surfaceFormats;
	std::vector<VkPresentModeKHR> surfacePresentModes;

	VkSurfaceFormatKHR chooseSurfaceFormat() {
		for (const auto& format : surfaceFormats) {
			if (format.format == VK_FORMAT_B8G8R8A8_SRGB
				&& format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
				return format;
			}
		}
		return surfaceFormats[0];
	}

	VkPresentModeKHR choosePresentMode() {
		for (const auto& mode : surfacePresentModes) {
			if (mode == VK_PRESENT_MODE_MAILBOX_KHR) {
				return mode;
			}
		}
		return VK_PRESENT_MODE_FIFO_KHR; // guaranteed to be avaliable
	}

	VkExtent2D chooseExtent(GLFWwindow* window) {
		if (surfaceCapabilities.currentExtent.width != UINT32_MAX) {
			return surfaceCapabilities.currentExtent;
		}
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);
		VkExtent2D actualExtent{ width, height };
		VkExtent2D extent;
		extent.width = std::clamp(actualExtent.width,
			surfaceCapabilities.minImageExtent.width,
			surfaceCapabilities.maxImageExtent.width);
		extent.height = std::clamp(actualExtent.height,
			surfaceCapabilities.minImageExtent.height,
			surfaceCapabilities.maxImageExtent.height);
		return extent;
	}
};

struct QueueFamilyIndices {
	std::optional<uint32_t> graphicsFamily;
	std::optional<uint32_t> presentationFamily; //mostly same as graphics family
	bool isComplete() {
		return graphicsFamily.has_value() && presentationFamily.has_value();
	}
};

class VulkanBase {
protected:
	GLFWwindow* window;

	VkInstance instance;

	VkPhysicalDevice physicalDevice;
	VkDevice device;

	VkQueue graphicsQueue;
	VkQueue presentQueue;

	VkSurfaceKHR surface;

	VkSwapchainKHR swapchain;
	VkFormat swapchainImageFormat;
	VkExtent2D swapchainExtent;

	VkCommandPool commandPool;

	std::vector<VkImage> swapchainImages;

	std::vector<VkCommandBuffer> commandBuffers;

	const int MAX_FRAMES_IN_FLIGHT = 20;

	// Sync objects
	std::vector<VkSemaphore> swapchainImageSemaphores;
	std::vector <VkSemaphore> renderSemaphores;
	std::vector<VkFence> frameFences;
	std::vector<VkFence> renderFences; //copied from frameFences depending on what image the frame is rendered to

	//static const uint32_t W_WIDTH = 1000;
	//static const uint32_t W_HEIGHT = 600;
	const uint32_t defaultWinWidth = 1024;
	const uint32_t defaultWinHeight = 650;

	uint32_t winWidth = defaultWinWidth;
	uint32_t winHeight = defaultWinHeight;

#ifdef NDEBUG
	bool enableValidationLayers = false;
#else
	bool enableValidationLayers = true;
#endif

	std::vector<const char*> deviceExtentions = {
			VK_KHR_SWAPCHAIN_EXTENSION_NAME,
			VK_NV_RAY_TRACING_EXTENSION_NAME,
			VK_KHR_MAINTENANCE3_EXTENSION_NAME,
			VK_EXT_DESCRIPTOR_INDEXING_EXTENSION_NAME
	};

	const std::vector<const char*> validationLayers = { "VK_LAYER_KHRONOS_validation" };

	class SceneNode {

		std::list<SceneNode> children;

		void eval(glm::mat4& parentTransform) {
			transform = parentTransform * transformLocal;
			for (auto& child : children) {
				child.eval(transform);
			}
		}

	public:
		glm::mat4 transformLocal = glm::mat4(1.0f);

		glm::mat4 transform = glm::mat4(1.0f);

		SceneNode& addChild() {
			return children.emplace_back();
		}

		SceneNode& addChild(glm::vec3 pos) {
			SceneNode& node = children.emplace_back();
			node.transformLocal = glm::translate(glm::mat4(1.0f), pos);
			return node;
		}

		void evalRoot() {
			glm::mat4 root(1.0f);
			eval(root);
		}
	};

	struct Camera {

		float fovY = glm::half_pi<float>();
		float fovXYRatio = 1.0f;
		float near = 0.1f;
		float far = 10.0f;

		float yaw = 0.0f;
		float pitch = 0.0f;

		float orbitDistance = 3.0f;

		const float panRate = 4;

		SceneNode* node;

		glm::mat4 getView() {
			return glm::inverse(getBase());
		}

		glm::mat4 getProj() {
			return glm::perspective(glm::radians(fovY), fovXYRatio, near, far);
		}

		static inline float fitAngle(float angle) {
			int fullDeg = 360 * (int)(angle / 360);
			if (angle >= 360) {
				return angle - fullDeg;
			}
			if (angle < 0) {
				return angle + fullDeg + 360;
			}
			return angle;
		}

		void rotateByScreenNormDelta(glm::vec2 delta) {
			float fovX = fovXYRatio * fovY;
			yaw = fitAngle(yaw + 180.f * delta.x);
			pitch = fitAngle(pitch + 180.f * delta.y);

			runningInstance->sceneUpdated = true;
		}

		void mouseScroll(float offset, bool toggle=false) {
			if (toggle) {
				orbitDistance += offset * 0.2f;
			}
			else {
				fovY -= offset * 0.1;
				if (fovY <= 0.2f) {
					fovY = 0.2f;
				}
				if (fovY >= glm::pi<float>()) {
					fovY = glm::pi<float>() - 0.001;
				}
			}
			runningInstance->sceneUpdated = true;
		}

		void mouseMove(glm::vec2 delta, bool toggle = false) {
			delta.y = -delta.y;
			if (toggle) {
				delta *= panRate;
				moveCamSpace(glm::vec3(1.f, 0.f, 0.f) * delta.x + glm::vec3(0.f, 1.f, 0.f) * delta.y);
			}
			else {
				rotateByScreenNormDelta(delta);
			}
			runningInstance->sceneUpdated = true;
		}

		glm::mat4 getBase() {
			glm::mat4 base;
			float y = glm::radians(yaw);
			float p = glm::radians(pitch);

			base[1] = glm::vec4(glm::sin(y) * glm::sin(p), glm::cos(p), glm::sin(p) * glm::cos(y), 0.f); //y
			base[2] = glm::vec4(glm::sin(y) * glm::cos(p), -glm::sin(p), glm::cos(p) * glm::cos(y), 0.f); //z
			base[0] = glm::vec4(glm::cross(glm::vec3(base[2]), glm::vec3(base[1])), 0.f); // x
			base[3] = node->transform[3] + base[2] * orbitDistance;
			return base;
		}

		void moveCamSpace(glm::vec3 delta) {
			node->transformLocal = glm::translate(node->transformLocal, glm::mat3(getBase()) * delta);
			runningInstance->sceneUpdated = true;
		}
	};

	struct Buffer {
		VkBuffer buffer;
		VkDeviceMemory memory;
		uint32_t count;

		void destroy(VkDevice device) {
			vkDestroyBuffer(device, buffer, nullptr);
			vkFreeMemory(device, memory, nullptr);
		}
	};

	struct SceneObject {
		std::list<SceneNode*> nodes;

		VkDevice device;

		size_t numGeometryTriangles;

		SceneObject() {
			device = runningInstance->device;
		}

		size_t numUniqueTriangles() {
			return nodes.empty() ? 0 : numGeometryTriangles;
		}

		size_t numInstanceTriangles() const {
			return numGeometryTriangles * nodes.size();
		}

		template<class T>
		void createElementsBuffer(Buffer& buffer, const std::vector<T>& elements, int usageBits) {
			buffer.count = elements.size();
			VkDeviceSize size = elements.size() * sizeof(T);
			VkBuffer stagingBuffer;
			VkDeviceMemory stagingBufferMemory;
			runningInstance->createBuffer(size,
				VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
				VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
				stagingBuffer, stagingBufferMemory);

			void* data;
			vkMapMemory(device, stagingBufferMemory, 0, size, 0, &data);
			memcpy(data, elements.data(), size);
			vkUnmapMemory(device, stagingBufferMemory);

			runningInstance->createBuffer(size,
				VK_BUFFER_USAGE_TRANSFER_DST_BIT | usageBits,
				VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
				buffer.buffer, buffer.memory);

			runningInstance->copyBuffer(buffer.buffer, stagingBuffer, size);

			vkDestroyBuffer(device, stagingBuffer, nullptr);
			vkFreeMemory(device, stagingBufferMemory, nullptr);
		}

		virtual ~SceneObject() {}
	};

	struct LightSource {
		std::list<SceneNode*> nodes;
		glm::vec3 color;
		uint32_t isDirectional;
	};

	struct Scene {
		std::unique_ptr<SceneNode> sceneRoot;
		std::list<std::unique_ptr<SceneObject>> sceneObjects;
		std::unique_ptr<Camera> camera;
		std::list<LightSource> lightSources;
		std::unique_ptr<Texture> envProbe;
		size_t numTriangles;
		size_t numUniqueTriangles;

		void init(SceneType type, bool useLightSources) {

			sceneRoot = std::make_unique<SceneNode>();

			auto rect = std::unique_ptr<GeometryObject>(GeometryObject::makeRectangle(5));
			rect->material.specular = 0.2;
			rect->material.sharpness = 10;
			SceneObject* so4 = sceneObjects.emplace_back(createSceneObject(*rect)).get();
			so4->nodes.push_back(sceneRoot.get());

			Camera cam;
			cam.node = sceneRoot.get();
			cam.far = 50.f;
			cam.orbitDistance = 3.2f;
			cam.fovXYRatio = runningInstance->winWidth / (float)runningInstance->winHeight;
			cam.pitch = 21.f;

			if (type == SceneType::MONKEYS) {
				GeometryObject geometry("monkey");

				for (int i = 0; i < 6; ++i) {
					auto& obj = sceneRoot->addChild();
					obj.transformLocal = glm::scale(obj.transformLocal, glm::vec3(0.5f));
					obj.transformLocal = glm::translate(obj.transformLocal, { 3.f * i - 7.5f, 1.5f, 0.f });

					geometry.material.specular = 0.2f * i;
					geometry.material.diffuse = glm::vec4(1.f) * (1.f - geometry.material.specular) / 3.f;
					geometry.material.sharpness = 10;
					auto* so = sceneObjects.emplace_back(createSceneObject(geometry)).get();

					so->nodes.push_back(&obj);
				}
				cam.orbitDistance = 4.f;
				cam.yaw = 195.f;
				cam.moveCamSpace(glm::vec3(-0.25f, 0.0f, 0.0f));
			}

			if (type == SceneType::BUNNY) {
				SceneObject* so1;
				SceneObject* so11;
				SceneObject* so2;
				SceneObject* so3;
				{
					GeometryObject geometry("buddha", true);
					geometry.material.diffuse = glm::vec4(0.8f, 0.1f, 0.1f, 1.0f);
					geometry.material.specular = 0.;
					geometry.material.sharpness = 1;
					so1 = sceneObjects.emplace_back(createSceneObject(geometry)).get();
					//so11 = sceneObjects.emplace_back(createSceneObject(geometry)).get();
				}

				{
					GeometryObject geometry("trash_can");
					geometry.material.specular = 0.;
					geometry.material.sharpness = 1;
					so2 = sceneObjects.emplace_back(createSceneObject(geometry)).get();
				}

				{
					GeometryObject geometry("bunny", true);
					geometry.material.diffuse = glm::vec4(0.1f, 0.1f, 0.1f, 1.0f);
					geometry.material.specular = 0.6;
					geometry.material.sharpness = 1;
					so3 = sceneObjects.emplace_back(createSceneObject(geometry)).get();
				}

				auto& obj1 = sceneRoot->addChild();
				obj1.transformLocal = glm::rotate(obj1.transformLocal, glm::pi<float>(), glm::vec3(0.0f, 1.0f, 0.0f));
				obj1.transformLocal = glm::scale(obj1.transformLocal, glm::vec3(2.0f, 2.0f, 2.0f));
				obj1.transformLocal = glm::translate(obj1.transformLocal, glm::vec3(.7f, .5f, .1f));
				auto& obj2 = sceneRoot->addChild();
				obj2.transformLocal = glm::rotate(obj2.transformLocal, glm::pi<float>(), glm::vec3(0.0f, 1.0f, 0.0f));
				obj2.transformLocal = glm::scale(obj2.transformLocal, glm::vec3(2.0f, 2.0f, 2.0f));
				obj2.transformLocal = glm::translate(obj2.transformLocal, glm::vec3(-.6f, .5f, .1f));

				auto& obj3 = sceneRoot->addChild({ 1.5f, 0.8f, 2.0f });

				auto& obj4 = sceneRoot->addChild();
				obj4.transformLocal = glm::scale(obj4.transformLocal, glm::vec3(1.1f, 1.1f, 1.1f));
				obj4.transformLocal = glm::translate(obj4.transformLocal, glm::vec3(-.0f, .0f, .0f));


				so1->nodes.push_back(&obj1);
				so1->nodes.push_back(&obj2);
				//so2->nodes.push_back(&obj3);
				so3->nodes.push_back(&obj4);

				cam.yaw = 180.f;
				cam.orbitDistance = 2.f;
				cam.pitch = 15.f;
				cam.moveCamSpace(glm::vec3(0.1f, -.3f, 0.0f));
			}

			if (type == SceneType::BUDDHAS) {
				GeometryObject geometry("buddha", true);
				geometry.material.diffuse = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
				geometry.material.specular = 0.2;
				geometry.material.sharpness = 5;

				for (int i = 0; i < 2; ++i) {
					for (int j = 0; j < 2; ++j) {
						auto& obj = sceneRoot->addChild();
						obj.transformLocal = glm::scale(obj.transformLocal, glm::vec3(2.f));
						obj.transformLocal = glm::translate(obj.transformLocal, { .75f * i - 0.5f, 0.5f, .75f * j - 0.5f });

						auto* so = sceneObjects.emplace_back(createSceneObject(geometry)).get();

						so->nodes.push_back(&obj);
					}
				}
				cam.moveCamSpace(glm::vec3(0.f, -.3f, 0.0f));
			}

			if (type == SceneType::INSTANCED_BUDDHAS) {
				GeometryObject geometry("buddha", true);
				geometry.material.diffuse = glm::vec4(1.f, 0.0f, 0.0f, 1.0f);
				geometry.material.specular = 0.2;
				geometry.material.sharpness = 5;
				auto* so = sceneObjects.emplace_back(createSceneObject(geometry)).get();

				for (int i = 0; i < 2; ++i) {
					for (int j = 0; j < 2; ++j) {
						auto& obj = sceneRoot->addChild();
						obj.transformLocal = glm::scale(obj.transformLocal, glm::vec3(2.f));
						obj.transformLocal = glm::translate(obj.transformLocal, { .75f * i - 0.5f, 0.5f, .75f * j - 0.5f });
						so->nodes.push_back(&obj);
					}
				}
				cam.moveCamSpace(glm::vec3(0.f, -.3f, 0.0f));
			}
			camera = std::make_unique<Camera>(cam);

			for (const auto& so : sceneObjects) {
				numUniqueTriangles += so->numUniqueTriangles();
				numTriangles += so->numInstanceTriangles();
			}

			if (useLightSources) {
				auto& objLight1 = sceneRoot->addChild({ 1.0f, 1.0f, 1.0f });

				LightSource& light1 = lightSources.emplace_back();
				light1.nodes.push_back(&objLight1);
				light1.color = glm::vec3(1.f, 1.f, 1.f) * 0.5f;
				light1.isDirectional = true;

				auto& objLight2 = sceneRoot->addChild({ -1.0f, 1.0f, 1.0f });

				LightSource& light2 = lightSources.emplace_back();
				light2.nodes.push_back(&objLight2);
				light2.color = glm::vec3(1.f, 1.f, 1.f) * 0.5f;
				light2.isDirectional = true;

				/*LightSource& light2 = lightSources.emplace_back();
				light2.nodes.push_back(&objLight1);
				//light2.color = glm::vec3(0.8f, 0.6f, 0.4f);
				light2.color = glm::vec3(1.f, 1.0f, 1.0f);
				light2.isDirectional = true;*/
			}

			envProbe = std::make_unique<Texture>("light-probes/uffizi_probe.hdr");

			sceneRoot->evalRoot();
		}

		virtual SceneObject* createSceneObject(const GeometryObject& geomObject) = 0;
	};

	std::unique_ptr<Scene> scene;

	VkQueryPool timeQueryPool;

	Log log;

	Arguments arguments;

	static void keyPressed(GLFWwindow* window, int key, int scancode, int action, int mods) {
		runningInstance->onKeyAction(key, action);
	}

	static void mouseMove(GLFWwindow* window, double xpos, double ypos);

	static void mouseScroll(GLFWwindow* window, double xOffset, double yOffset) {
		runningInstance->scene->camera->mouseScroll(yOffset, glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS);
	}

	bool windowResized = false;

	static void resizedCallback(GLFWwindow* window, int width, int height) {
		runningInstance->winWidth = width;
		runningInstance->winHeight = height;
		runningInstance->windowResized = true;
	}

	virtual void recordPipelineDrawCommands(VkCommandBuffer commandBuffer, int swapchainIndex) = 0;

	virtual void frameUpdate(int swapchainIndex) = 0;

	virtual void createSwapchainDeps() = 0;

	virtual void cleanupSwapchainDeps() = 0;

	virtual void printProfilingInfo() = 0;

	virtual void onKeyAction(int key, int action) {}

	void createInstance();

	void createSurface() {
		log.check_failure(glfwCreateWindowSurface(instance, window, nullptr, &surface),
			"Failed to create window surface!");
	}

	VkPhysicalDeviceProperties physDeviceProperties;

	void pickDevice();
	
	SwapchainProperties getSwapchainProperties();

	void createSwapchain(bool useOld);

	void createLogicalDevice();

	bool checkSupport(const std::vector<const char*>& requested, const std::vector<const char*>& supported);

	QueueFamilyIndices getDeviceQueueFamilyIndices();

	bool checkDeviceExtsSupport(const std::vector<const char*>& requested);

	bool checkGlobalExtsSupport(const std::vector<const char*>& requested);

	bool checkValidationLayersSupport();

	VkShaderModule createShaderModule(const std::vector<char>& binary);

	uint32_t chooseMemoryType(uint32_t filter, VkMemoryPropertyFlags properties);

	void createCommandPool();

	void createCommandBuffers();

	void createSyncObjects();

	void createTimestampQueryPool(VkQueryPool& pool);

	bool sceneUpdated = true;

	void onResized(int imageIndex);

	struct FramesTimeSpan {
		unsigned long framesCnt = 0;
		unsigned long gpuTimeMs = 0;
		unsigned long cpuTimeMs = 0;
		FramesTimeSpan operator -(FramesTimeSpan other) {
			return {
				framesCnt - other.framesCnt,
				gpuTimeMs - other.gpuTimeMs,
				cpuTimeMs - other.cpuTimeMs
			};
		}
	};

	FramesTimeSpan renderState;
	FramesTimeSpan recentFramesTime;

	void drawFrame();

	void frameUpdateBase(uint32_t imageIndex) {
		//Update total time
		int gpuFrameTime = getGPUFrameTimeMs(timeQueryPool, imageIndex);
		if (gpuFrameTime > -1) {
			renderState.gpuTimeMs += gpuFrameTime;
		}
		renderState.cpuTimeMs = getRenderTimeMs();

		frameUpdate(imageIndex);

		static FramesTimeSpan lastFrameTime = renderState;
		//Update the timings for profiling period
		static unsigned long lastProfiled = 0;
		unsigned long curTime = renderState.cpuTimeMs / 1000;
		if (lastProfiled != curTime && (curTime % arguments.profilingPeriodSec == 0 || sceneUpdated)) {
				recentFramesTime = renderState - lastFrameTime;
				lastFrameTime = renderState;
				lastProfiled = curTime;
				printProfilingInfo();
		}

		if (sceneUpdated) {
			scene->sceneRoot->evalRoot();
			sceneUpdated = false;
		}
	}

	std::chrono::high_resolution_clock::time_point renderStart;

	int getRenderTimeMs() {
		auto now = std::chrono::high_resolution_clock::now();
		return round(std::chrono::duration<float, std::chrono::milliseconds::period>(now - renderStart).count());
	}

	int getGPUFrameTimeMs(VkQueryPool timeQueryPool, int imageInd) {
		struct {
			uint64_t start;
			uint64_t startAval;
			uint64_t end;
			uint64_t endAval;
		}timeSpan;
		vkGetQueryPoolResults(device, timeQueryPool, imageInd * 2, 2, sizeof(timeSpan), &timeSpan, sizeof(uint64_t) * 2,
			VK_QUERY_RESULT_64_BIT | VK_QUERY_RESULT_WITH_AVAILABILITY_BIT);
		if (timeSpan.startAval != 0 && timeSpan.endAval != 0) {
			return round((timeSpan.end - timeSpan.start) * physDeviceProperties.limits.timestampPeriod / 1000'000.0f);
		}
		return -1;
	}

	void initWindow();

	void initVulkan();

	void runInit();

	~VulkanBase();

	void runCleanup();

	void cleanupSyncObjects();

	bool renderTerminated = false;

public:

	VkAccessFlags getImageLayoutAccessFalgs(VkImageLayout imageLayout) {
		switch (imageLayout)
		{
		case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
			return VK_ACCESS_TRANSFER_READ_BIT;
		case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
			return VK_ACCESS_TRANSFER_WRITE_BIT;
		default:
			return 0;
		}
	}

	void transferImageLayout(VkCommandBuffer commandBuffer,
		VkImage image,
		VkImageLayout oldLayout,
		VkImageLayout newLayout);

	void createImage(int width, int height,
		VkFormat format,
		VkImageUsageFlags imageUsage,
		VkImage& image,
		VkDeviceMemory& imageMemory);

	VkImageView createImageView(VkImage image, VkFormat format);

	VkCommandBuffer beginNewCommandBuffer();

	void endAndFlushCommandBuffer(VkCommandBuffer commandBuffer);

	void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags propertyFlags,
		VkBuffer& buffer, VkDeviceMemory& bufferMemory);

	void copyBuffer(VkBuffer dst, VkBuffer src, VkDeviceSize size);

	static VulkanBase* runningInstance;

	VulkanBase(Arguments arguments);

	void run();
};